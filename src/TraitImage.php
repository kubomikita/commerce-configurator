<?php
namespace Kubomikita\Core;

use Nette\InvalidStateException;
use Nette\Utils\FileSystem;
use Nette\Utils\Strings;

trait TraitImage {

	protected $type = \ImageRecord::TYPE_JPEG;
	/**
	 * @var content
	 * @global content
	 */
	public $content;
	/**
	 * @var nazov
	 * @global nazov
	 */
	public $nazov;
	/**
	 * TraitImage constructor.
	 *
	 * @param null $data
	 * @param null $name
	 */
	public function __construct( $data = null, $name=null) {
		$this->initImageTrait();
		parent::__construct( $data );
		if($name !== null){
			$this->nazov = $name;
		}
	}

	private function initImageTrait(){
		if(!isset(static::$imageName)){
			throw new InvalidStateException("Static property '\$imageName' must be defined when using Trait 'Image'.");
		}
	}

	/**
	 * @return string
	 */
	private function getImageName() : string {
		return static::$imageName;
	}

	/**
	 * @param string $type
	 *
	 * @return $this
	 */
	public function setType( string $type ) {
		$this->type = $type;
		return $this;
	}

	/**
	 * @return string
	 */
	public function storageFile() : string {
		if(isset(static::$imageBin) && static::$imageBin != null){
			return WWW_DIR."bindata/".static::$imageBin."-".$this->id.".".$this->type;
		}
		return WWW_DIR."bindata/".$this->getImageName()."-".$this->id.".".$this->type;
	}

	/**
	 * @return string
	 */
	public function binUrl() : string {
		if(isset(static::$imageBin) && static::$imageBin != null){
			return "bindata/".static::$imageBin."-".$this->id.".".$this->type;
		}
		return "bindata/".$this->getImageName()."-".$this->id.".".$this->type;
	}

	/**
	 * @return string
	 */
	public function url() : string {
		return 'img'.substr($this->getImageName(),0,1).'/'.$this->id.'.'.$this->type.autoVersion($this->binUrl(),true);
	}

	/**
	 * @return string
	 */
	public function cachedUrl() : string {
		return cdnImgCache('imgcache/c-'.$this->getImageName().'-'.$this->id.'.'.$this->type.autoVersion($this->binUrl(),true));
	}

	/**
	 * @return string
	 */
	public function cachedUrlName() : string {
		if($this->nazov !== null) {
			return cdnImgCache( 'imgcache/' . Strings::webalize( $this->nazov ) . "-full-".$this->getImageName()."-" . $this->id . ".".$this->type . autoVersion( $this->binUrl(),
					true ) );
		}
		return $this->cachedUrl();
	}


	/**
	 * @param $method
	 * @param int $sx
	 * @param int $sy
	 * @param string|null $color
	 *
	 * @return string
	 */
	public function resizedUrl($method,$sx, $sy, $color = null) : string {
		$color = $this->getColor($color);
		$mid = $this->getResizeMethod($method);
		return 'img'.substr($this->getImageName(),0,1).'/'.$this->id.'.'.$this->type.'?resize='.$mid.'&sx='.$sx.'&sy='.$sy.'&color='.$color; //.autoVersion($this->binUrl(),true);
	}

	/**
	 * @param $method
	 * @param int $sx
	 * @param int $sy
	 * @param string|null $color
	 *
	 * @return string
	 */
	public function resizedCachedUrl($method,$sx, $sy, $color = null) : string {
		$color = $this->getColor($color);
		$mid = $this->getResizeMethod($method);
		return cdnImgCache('imgcache/c-'.$this->getImageName().'-'.$this->id.'-'.$mid.'-'.$sx.'-'.$sy.'--'.$color.'.'.$this->type.autoVersion($this->binUrl(),true));
	}

	/**
	 * @param $method
	 * @param int $sx
	 * @param int $sy
	 * @param string|null $color
	 *
	 * @return string
	 */
	public function resizedCachedUrlName($method, $sx, $sy,  $color = null) : string {
		$color = $this->getColor($color);
		$mid = $this->getResizeMethod($method);
		return cdnImgCache('imgcache/'.Strings::webalize($this->nazov)."-resized-".$this->getImageName()."-".$this->id.'-'.$mid.'-'.$sx.'-'.$sy.'-'.$color.'.'.$this->type.autoVersion($this->binUrl(),true));
	}
	/**
	 * TODO: This
	 */
	public function cachedNoWatermarkUrl(){
		return "";
		//return cdnImgCache('imgcache/cx-item-'.$this->id.'-'.strtr(substr(md5($this->id),0,8),'abcdef','123456').'.'.$this->type.autoVersion($this->binUrl(),true));
	}
	/**
	 * @param $color
	 *
	 * @return string
	 */
	private function getColor($color) : string {
		if($color !== null){
			$color=strtr($color,array('#'=>''));
		}
		return (string) $color;
	}

	/**
	 * @param mixed $method
	 *
	 * @return int
	 */
	private function getResizeMethod($method) : int {
		if(!is_int($method)){
			$Methods=array('scale'=>1,'fit'=>3,'crop'=>5,'max'=>4);
			return $Methods[$method];
		}
		return $method;
	}

	public function saveData(){
		if($this->id){
			if(\Config::useImgFileStore){
				file_put_contents($this->storageFile($this->id),$this->content);
			} else {
				$this->db->query("UPDATE ".static::$table,["img"=>$this->context],"WHERE id = ?",$this->id);
			};
		};
	}
	public function dataLoad(){
		if(\Config::useImgFileStore){
			$this->content = file_get_contents($this->storageFile($this->id));
		} else {
			$q = $this->db->query("SELECT * FROM ".static::$table." WHERE id = ?", $this->id)->fetch();
			if($q){
				$this->content=$q->content;
			};
		};
	}

	public function deleteImageTrait(){
		\Cache::flush(static::$table);
		if(isset(static::$table_parent)){
			\Cache::flush(static::$table_parent);
		}
	}
	public function saveImageTrait(){
		\Cache::flush(static::$table);
		if(isset(static::$table_parent)){
			\Cache::flush(static::$table_parent);
		}
		//
	}

}