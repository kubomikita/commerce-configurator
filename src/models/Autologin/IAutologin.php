<?php
interface IAutologin {

	public function getUsername();
	public function getFullname();
	public function getEmail();
	public function notifyLogin();
	public function checkDevice();

	/**
	 * @param bool $new
	 *
	 * @return stdClass|static
	 */
	public static function detect($new = false);
	public static function checkHash($hash);
	public static  function unactive($hash);
	public static function create($partner_id,$hash);
}