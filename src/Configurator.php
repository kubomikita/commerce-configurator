<?php


namespace Kubomikita\Commerce;

use Nette;
use Nette\DI;
use Tracy;

use Kubomikita\Service;
use Nette\Application\AbortException;
use Nette\Neon\Neon;
use Nette\Utils\FileSystem;
use Nette\Utils\Strings;

abstract class Configurator implements IConfigurator {
	public $controllers = [];
	const COOKIE_SECRET = "commerce_debug";
	/** @var array */
	protected $parameters;
	/** @var array */
	protected $register = [
		"latte" => "LatteService",
		"url"=>"UrlService",
		"request"=>"RequestService",
		"response"=>"ResponseService",
		"session"=>"SessionService",
		"user"=>"UserService",
		"gettext"=>"GettextService",
		"database" => "DatabaseService",
		"form-factory"=> "KubomikitaFormService",
		"flash-factory" => "KubomikitaFlashService",
		"microdata" => "MicrodataService",
		"mail" => "MailService",
	];
	/** @var array  */
	protected $files = [];
	/** @var array  */
	protected $cliServices = [
		"database",
		"gettext"
	];
	/** @var array */
	protected $routers = [
		"cli" => "\\Kubomikita\\Cli\\BaseRouter",
		"redirects" => "\\Kubomikita\\Commerce\\RedirectsRouter",
		"ajax" => "\\Kubomikita\\Ajax\\Router",
		"api" => "\\Kubomikita\\Api\\Router",
		"application" => "\\Kubomikita\\Commerce\\Router",
	];
	/** @var array */
	public $container = [];
	/** @var bool */
	protected $debuggerEnabled = false;
	/** @var string */
	protected $debuggerPanelService = "\\Kubomikita\\Commerce\\TracyPanelService";

	/**
	 * Set class name of CLI router
	 * @param $class string
	 */
	public function setCliRouter($class){
		$this->routers["cli"] = $class;
	}

	/**
	 * Set class name of API router
	 * @param $class string
	 */
	public function setApiRouter($class){
		$this->routers["api"] = $class;
	}

	/**
	 * Set class name of Application router
	 * @param $class string
	 */
	public function setAppRouter($class){
		$this->routers["application"] = $class;
	}
	public function setAjaxRouter($class){
		$this->routers["ajax"] = $class;
	}
	public function setRedirectsRouter($class){
		$this->routers["redirects"] = $class;
	}
	public function loadService($c){
		if(isset($this->register[$c])){
			$class = "\\Kubomikita\\Commerce\\".$this->register[$c];
			if(class_exists($class)){
				$factory = new $class();
				if($factory instanceof IService) {
					${$c} = $factory->startup( $this );
					if ( ! is_array( ${$c} ) ) {
						Service::add( $c, ${$c} );
						$this->container[ $c ] = ${$c};
					} else {
						$objectarray = ${$c};
						foreach ( $objectarray as $k => $v ) {
							$this->container[ $k ] = $v;
							Service::add( $k, $v );
						}
					}
				} else {
					throw new \Exception("Service not implemets IService");
				}
			} else {
				throw new \Exception("Service '$class' not exists.");
			}
		} else {
			throw new \Exception("Service for key '$c' not created.");
		}
	}

	/**
	 * @param array $list
	 *
	 * @throws \Exception
	 */
	public function createRegister(array $list = []){
		$defaultList = ["database","url","request","response","session","user","gettext","latte"];
		$defaultList = array_merge($defaultList,$list);
		if(!$this->parameters["consoleMode"]){
			try {
				foreach($defaultList as $c){
					$this->loadService($c);
				}
			} catch(\Exception $e){
				trigger_error($e->getMessage(),E_USER_ERROR);

			}
		} else {
			foreach($this->cliServices as $c){
				if(in_array($c,$defaultList)){
					$this->loadService($c);
				}
			}
		}
		Service::add("container",$this);
		Service::add("configurator",$this);
		$this->controllers = \CommerceController::startup();
	}

	/**
	 * @param Page $Page
	 * @param AppRouter $Router
	 *
	 * @return string
	 */
	abstract public function run( $Page, $Router );
	/**
	 * Creates routers
	 * @param array $list
	 *
	 * @throws Nette\Application\BadRequestException
	 */
	public function createRouter(array $list){
		if($this->isCli()){
			if(!in_array("cli",$list)){
				throw new Nette\Application\BadRequestException("Cli Router not defined.");
			}
		}
		foreach($list as $key){
			if(!isset($this->routers[$key])){
				throw new Nette\Application\BadRequestException("Router with '$key' not exists.",500);
			}
			$method = "router".Nette\Utils\Strings::firstUpper($key);
			$param = $this->routers[$key];
			if(!method_exists($this,$method)){
				throw new Nette\Application\BadRequestException("Router init function not exists",500);
			}
			if(!class_exists($this->routers[$key])){
				throw new Nette\Application\BadRequestException("Router class '{$this->routers[$key]}' not exists.",500);
			}
			$this->{$method}($param);
		}
	}

	/**
	 * CLI router init
	 * @param $className string
	 */
	protected function routerCli($className){
		if ( $this->isCli() ) {
			$cli = new $className( $_SERVER['argv']);
			exit;
		}
	}

	/**
	 * API router init
	 * @param $classname string
	 *
	 * @throws Nette\Application\BadRequestException
	 */
	protected function routerApi($classname){
		if(!$this->isCli()) {
			if ( $this->getService( "request" )->getQuery( 'api' ) ) {
				$api = new $classname( $this->getService( "request" )->getQuery( "query" ) );
				exit;
			}
		}
	}

	protected function routerAjax($classname){
		if(!$this->isCli()){
			if($this->getService("request")->isAjax()){
				$ajax = new $classname($this);
				exit;
			}
		}
	}
	protected function routerRedirects($classname){
		if(!$this->isCli()){
			$redir = new $classname($this);
		}
	}
	/**
	 * Application router init
	 * @param $classname
	 */
	protected function routerApplication($classname){
		bdump($classname,"Application router");
	}
	public function __construct()
	{
		$this->parameters = $this->getDefaultParameters();
		if(!defined("APP_DIR") || !defined("WWW_DIR") || !defined("ASSETS_DIR") || !defined("TEMP_DIR")){
			throw new AbortException("Main dir paths is not defined!");
		}
	}

	public function createFunctionsLoader(array $list){
		foreach ( $list as $file )
		{
			if(!file_exists($file)){
				throw new \Exception("File '$file' not exists.");
			}
			include_once $file;
		}
	}
	/**
	 * Is host
	 * @param $host
	 *
	 * @return bool
	 */
	public function isHost($host){
		if(strpos($this->parameters["host"],$host) !== false){
			return true;
		}
		return false;
	}

	/**
	 * Is console mode
	 * @return bool
	 */
	public function isCli(){
		return $this->parameters["consoleMode"];
	}

	/**
	 * Set parameter %debugMode%.
	 * @param  bool|string|array
	 * @return self
	 */
	public function setDebugMode($value)
	{
		if (is_string($value) || is_array($value)) {
			$value = static::detectDebugMode($value);
		} elseif (!is_bool($value)) {
			throw new Nette\InvalidArgumentException(sprintf('Value must be either a string, array, or boolean, %s given.', gettype($value)));
		}
		$this->parameters['debugMode'] = $value;
		$this->parameters['productionMode'] = !$this->parameters['debugMode']; // compatibility
		$this->parameters['environment'] = $this->parameters['debugMode'] ? 'development' : 'production';
		return $this;
	}


	/**
	 * @return bool
	 */
	public function isDebugMode()
	{
		return $this->parameters['debugMode'];
	}
	//public function is


	/**
	 * Sets path to temporary directory.
	 * @return self
	 */
	public function setTempDirectory($path)
	{
		$this->parameters['tempDir'] = $path;
		return $this;
	}


	/**
	 * Adds new parameters. The %params% will be expanded.
	 * @return self
	 */
	public function addParameters(array $params)
	{
		$this->parameters = DI\Config\Helpers::merge($params, $this->parameters);
		return $this;
	}
	protected function getDefaultParameters()
	{
		$trace = debug_backtrace(PHP_VERSION_ID >= 50306 ? DEBUG_BACKTRACE_IGNORE_ARGS : FALSE);
		$last = end($trace);
		$debugMode = static::detectDebugMode();
		return array(
			'appDir' => isset($trace[1]['file']) ? dirname($trace[1]['file']) : NULL,
			'wwwDir' => isset($last['file']) ? dirname($last['file']) : NULL,
			'debugMode' => $debugMode,
			'productionMode' => !$debugMode,
			'environment' => $debugMode ? 'development' : 'production',
			'consoleMode' => PHP_SAPI === 'cli',
			"host" => (PHP_SAPI === "cli")?"cli":$_SERVER["SERVER_NAME"],
			'container' => array(
				'class' => NULL,
				'parent' => NULL,
			),
		);
	}

	/**
	 * @param $key [database,latte]
	 *
	 * @return mixed
	 * @throws Nette\Application\BadRequestException
	 */
	public function getService($key){
		if(!isset($this->container[$key])){
			throw new Nette\Application\BadRequestException("Service with key '$key' not exists.",500);
		}
		return $this->container[$key];
	}

	/**
	 * @param  string        error log directory
	 * @param  string        administrator email
	 * @return void
	 */
	public function enableDebugger($logDirectory = NULL, $email = NULL)
	{
		//Tracy\Debugger::$strictMode = TRUE;
		Tracy\Debugger::enable(!$this->parameters['debugMode'], $logDirectory, $email);
		Tracy\Debugger::$errorTemplate = APP_DIR."view/error500.phtml";
		$this->debuggerEnabled = true;
		if(/*$this->debuggerEnabled && */class_exists($this->debuggerPanelService)) {
			$tracyPanel = new $this->debuggerPanelService();
			$tracyPanel->startup( $this );
		}
	}

	public function afterAppLoad(){
		if($this->debuggerEnabled && class_exists($this->debuggerPanelService)){
			$tracyPanel = new $this->debuggerPanelService();
			$tracyPanel->afterLoad($this);
		}
	}


	/**
	 * @return Nette\Loaders\RobotLoader
	 * @throws Nette\NotSupportedException if RobotLoader is not available
	 */
	public function createRobotLoader()
	{
		if (!class_exists('Nette\Loaders\RobotLoader')) {
			throw new Nette\NotSupportedException('RobotLoader not found, do you have `nette/robot-loader` package installed?');
		}

		$loader = new Nette\Loaders\RobotLoader;
		$loader->setTempDirectory($this->getCacheDirectory());
		//$loader->setCacheStorage(new Nette\Caching\Storages\FileStorage($this->getCacheDirectory()));
		//$loader->autoRebuild = $this->parameters['debugMode'];
		return $loader;
	}
	public function getCacheDirectory()
	{
		if (empty($this->parameters['tempDir'])) {
			throw new Nette\InvalidStateException('Set path to temporary directory using setTempDirectory().');
		}
		$dir = $this->parameters['tempDir'] . '/cache';
		if (!is_dir($dir)) {
			@mkdir($dir); // @ - directory may already exist
		}
		return $dir;
	}
	/**
	 * Detects debug mode by IP address.
	 * @param  string|array  IP addresses or computer names whitelist detection
	 * @return bool
	 */
	public static function detectDebugMode($list = NULL)
	{
		if(isset($_SERVER["HTTP_X_REAL_IP"])){
			$addr = $_SERVER["HTTP_X_REAL_IP"];
		} else {
			$addr = isset( $_SERVER['REMOTE_ADDR'] )
				? $_SERVER['REMOTE_ADDR']
				: php_uname( 'n' );
		}
		$secret = isset($_COOKIE[self::COOKIE_SECRET]) && is_string($_COOKIE[self::COOKIE_SECRET])
			? $_COOKIE[self::COOKIE_SECRET]
			: NULL;
		$list = is_string($list)
			? preg_split('#[,\s]+#', $list)
			: (array) $list;
		if (!isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$list[] = '127.0.0.1';
			$list[] = '::1';
		}
		return in_array($addr, $list, TRUE) || in_array("$secret@$addr", $list, TRUE);
	}

	/**
	 * @return array
	 * @throws Nette\Application\BadRequestException
	 */
	protected function cookiesBar(){
		$REQUEST = $this->getService("request");
		$cookie = [];
		$cookie["setted"] = (bool) $REQUEST->getCookie("cookies_setted");
		$cookie["technical"] = (bool)$REQUEST->getCookie("allow_technical_cookies");
		$cookie["analytics"] = (bool)$REQUEST->getCookie("allow_analytics_cookies");
		$cookie["marketing"] = (bool)$REQUEST->getCookie("allow_marketing_cookies");
		$cookie["scripts"]["css"] = file_get_contents(__DIR__."/assets/kubomikita.cookiebar.css");
		$cookie["scripts"]["js"] = file_get_contents(__DIR__."/assets/kubomikita.cookiebar.js");

		return $cookie;
	}

	public function addConfig($file){
		$this->files[] = [$file,(strpos($file,"local") ? 'development' : 'production')];
		foreach($this->files as $f){
			$content = FileSystem::read($f[0]);
			if(strlen(trim($content))>0) {
				$neon = Neon::decode( $content );
				if(!$this->isDebugMode() and ($f[1] == "development") and !$this->isCli() ){
					continue;
				}
				$this->addParameters($neon);
			}
		}
	}

	/**
	 * @param null $key
	 *
	 * @return mixed
	 * @throws AbortException
	 */
	public function getParameter($section,$key = null){
		if(!isset($this->parameters[$section])){
			throw new AbortException("Section '$section' in parameters not exists");
		}
		if(!isset($this->parameters[$section][$key]) and $key !== null){
			throw new AbortException("Key '$key' in section '$section' not exists");
		}
		if($key !== null){
			return $this->parameters[$section][$key];
		}
		return $this->parameters[$section];
	}

	/**
	 * Check if app runing on localhost or production server
	 * @return bool
	 */
	public function isProduction(){
		if(!$this->isCli()) {
			return ( $_SERVER["HTTP_HOST"] != "localhost" );
		} else {
			if(strpos(__DIR__,"E:\\www") !== false){
				return false;
			}
		}
		return true;
	}

	public static function internationalPhoneNumber($number){
		$val = str_replace(" ","",$number);
		if(!Strings::contains($val,"+421")){
			$val = "+421".Strings::substring($val,1);
		}
		return $val;
	}
	public static function getRelativePath( $from, $to ) {
		// some compatibility fixes for Windows paths
		$from = is_dir( $from ) ? rtrim( $from, '\/' ) . '/' : $from;
		$to   = is_dir( $to ) ? rtrim( $to, '\/' ) . '/' : $to;
		$from = str_replace( '\\', '/', $from );
		$to   = str_replace( '\\', '/', $to );

		$from    = explode( '/', $from );
		$to      = explode( '/', $to );
		$relPath = $to;

		foreach ( $from as $depth => $dir ) {
			// find first non-matching dir
			if ( $dir === $to[ $depth ] ) {
				// ignore this directory
				array_shift( $relPath );
			} else {
				// get number of remaining dirs to $from
				$remaining = count( $from ) - $depth;
				if ( $remaining > 1 ) {
					// add traversals up to first matching dir
					$padLength = ( count( $relPath ) + $remaining - 1 ) * - 1;
					$relPath   = array_pad( $relPath, $padLength, '..' );
					break;
				} else {
					$relPath[0] = './' . $relPath[0];
				}
			}
		}

		return implode( '/', $relPath );
	}
}