<?php


namespace Kubomikita\Commerce;


interface IDiConfigurator extends IConfigurator {

	/**
	 * @param $section
	 * @param null $key
	 *
	 * @return array|mixed
	 */
	public function getParameter($section,$key = null);

	/**
	 * @param array $section
	 * @param $value
	 *
	 * @return IDiConfigurator
	 */
	public function setParameter(array $section,$value);
	/**
	 * Check if app runing on localhost or production server
	 * @return bool
	 */
	public function isProduction();

	/**
	 * Checks if app is protein.sk or protein.cz
	 * @return bool
	 */
	public function isProtein();

	public function getByType(string $class);
}