<?php

class VipZlava extends Zlava {
	use StandardDiscount;
	public $id = 0;
	public $suma_od = 0;
	public $zlava = 0;
	private $metadata;
	public $code = 'W';

	public function __construct() {
		$this->zlava = Config::vipZlava;
	}

	public function format(){
		$ret=sprintf("%.2F",$this->zlava).'%';
		return $ret;
	}

	public function koef(){
		$ret=(100-$this->zlava)/100;
		return $ret;
	}

	public function title(){ return LangStr('VIP ZĽAVA'); }
	public function titlefa() { return "VIP ZĽAVA";}
}