<?php
class Zlava {
	use StandardDiscount;
	public $id;
	public $suma_od = 0;
	public $zlava = 0;
	public $code = '';
	public $suma = 0;

	public function format(){
		$ret=sprintf("%.2F",$this->zlava).'%';
		return $ret;
	}

	public function koef(){
		$ret=(100-$this->zlava)/100;
		return $ret;
	}

	public function title(){ return LangStr('ZĽAVA'); }
	public function titlefa(){ return LangStr('ZĽAVA'); }
	public static function make($code,$zlava,$suma=0){
		$Z = new Zlava();
		if($code=='M'){ $Z = new MnozstevnaZlava(); };
		if($code=='V'){ $Z = new VernostnaZlava(); };
		if($code=='W'){ $Z = new VipZlava(); };
		if($code=="DP"){$Z = new DarcekovaPoukazka();}
		$Z->zlava=$zlava;
		$Z->suma = $suma;
		return $Z;
	}
}