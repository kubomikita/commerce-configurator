<?php

class DarcekovaPoukazka extends Zlava {
	use StandardDiscount;

	public $id = 0;
	public $suma_od = 0;
	public $suma = 0;
	public $zlava = 0;
	public $code = "DP";

	public $cupon = null;

	public function title(){ return LangStr('Darčeková poukážka'); }
	public function titlefa() { return "Vernostná zľava";}
	public function format(){
		if(Kosik::total_inclVAT() < $this->suma_od and Kosik::total_inclVAT()>0){
			Flash::danger("<span class=\"fa fa-times\"></span> Darčeková poukážka platí až pri <strong>objednávke nad ".$this->suma_od."€</strong>");
		}
		//return "-".number_format($suma,2)." €";
		if($this->cupon !== null){
			return "\"<strong>".$this->cupon."</strong>\"";
		}
		return "\"<strong>".Kosik::meta_get('promo_kod')."</strong>\"";
	}
	public function minus(){
		return -$this->suma;
	}
}