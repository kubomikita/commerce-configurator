<?php


namespace Kubomikita\Api;


use Exception;
use Kubomikita\Service;
use Nette\Application\AbortException;
use Nette\Application\ForbiddenRequestException;
use Nette\Application\Responses\FileResponse;
use Nette\Database\Connection;
use Nette\Database\ResultSet;
use Nette\Utils\DateTime;
use Nette\Utils\FileSystem;
use Nette\Utils\Image;
use Nette\Utils\Json;
use Nette\Utils\Strings;
use Prewk\XmlStringStreamer;
use Prewk\XmlStringStreamer\Parser\StringWalker;
use Prewk\XmlStringStreamer\Parser\UniqueNode;
use Prewk\XmlStringStreamer\Stream\File;
use Tracy\Debugger;

class Oberon extends BaseApi{

	protected $logRequests = true;
	protected $importDir = TEMP_DIR."/import";

	public function beforeStartup() {
		if(!$this->config->enabled){
			throw new AbortException("API is disabled.");
		}
		Debugger::$showBar = false;
	}

	public function authenticate(): void
	{
		parent::authenticate();
		$AUTH_USER = $this->config->http_auth_user;
		$AUTH_PASS = $this->config->http_auth_pass;
		header('Cache-Control: no-cache, must-revalidate, max-age=0');
		$has_supplied_credentials = !(empty($_SERVER['PHP_AUTH_USER']) && empty($_SERVER['PHP_AUTH_PW']));
		$is_not_authenticated = (
			!$has_supplied_credentials ||
			$_SERVER['PHP_AUTH_USER'] != $AUTH_USER ||
			$_SERVER['PHP_AUTH_PW']   != $AUTH_PASS
		);
		if ($is_not_authenticated) {
			header('HTTP/1.1 401 Authorization Required');
			header('WWW-Authenticate: Basic realm="Access denied"');
			exit;
		}
	}


	/**
	 * @param int $skipExported
	 * @return ResultSet
	 */
	public function getOrders(int $skipExported) : ResultSet
	{
		$this->db = Service::getByType(Connection::class);
		if($skipExported) {
			return $this->db->query( "SELECT * FROM ec_objednavky WHERE export = 0 LIMIT 10" );//ORDER BY id DESC LIMIT 20
		}
		return  $this->db->query("SELECT * FROM ec_objednavky ORDER BY id DESC LIMIT 20");
	}

	/**
	 * @param int $skipExported
	 * @param int $mksoftRequest
	 *
	 * @throws Exception
	 */
	public function actionExportOrders(int $skipExported = 1, int $oberonRequest = 0) : void {
		$domtree = new \DOMDocument("1.0","UTF-8");

		$xmlRoot = $domtree->createElement("OBERON");
		//$xmlRoot->setAttribute("popis","export z e-shopu");
		//$xmlRoot->setAttribute("verzia", 2);
		//$xmlRoot->setAttribute("kontakt","");
		$xmlRoot = $domtree->appendChild($xmlRoot);

		$header = $domtree->createElement("Header");
		$header->appendChild($domtree->createElement("DocumentType", 101)); //1050
		$header->appendChild($domtree->createElement("DocumentDateTime", (new DateTime())->format("d.m.Y H:i:s")));
		$header->appendChild($domtree->createElement("CompanyName", "Wilde s.r.o."));
		$header->appendChild($domtree->createElement("OBERONVersion", "Jún/2021"));
		$header->appendChild($domtree->createElement("OBERONVersionDB", 401));

		//<OBERONVersion>December/2012 Beta1</OBERONVersion><!-- verzia OBERON na ktorej bol vykonaný export -->
		//<OBERONVersionDB>170</OBERONVersionDB>
		$header->appendChild($domtree->createElement("User", "Admin"));
		$xmlRoot->appendChild($header);

		$settings = $domtree->createElement("Settings");
		$settings->appendChild($domtree->createElement("Type", 1));
		$settings->appendChild($domtree->createElement("TypeText", "Všetky"));
		$xmlRoot->appendChild($settings);

		$data = $domtree->createElement("Data");
		$list = $data->appendChild($domtree->createElement("OrdersReceived"));

		$xmlRoot->appendChild($data);

		$orders = $this->getOrders($skipExported);
		$ORDERS = [];
		if($orders->fetch()) {
			foreach ( $orders as $order ) {
				$O = new \Objednavka( $order );
				/*if ( $O->isOnlinePayment() ) {
					if ( ! $O->isPaid() ) {
						continue;
					}
				}*/
				$ORDERS[] = $O;
			}
		}
		//$ORDERS = [];
		$ids = [];
		/** @var \Objednavka $O */
		foreach ( $ORDERS as $O ) {
			$db_user             = [
				"id" => $O->klient->id + 100000,
				"name"     => $O->klient->kontakt["meno"],
				"surname"  => $O->klient->kontakt["priezvisko"],
				"firm"     => ( ! $O->klient->fo ) ? $O->klient->format_nazov() : $O->klient->kontakt["meno"]." ".$O->klient->kontakt["priezvisko"], //"J & M & co.",
				"street"   => $O->klient->kontakt["adresa_ulica"] . " " . $O->klient->kontakt["adresa_cislo"],
				"number"   => $O->klient->kontakt["adresa_cislo"],
				"city"     => $O->klient->kontakt["mesto"] ? $O->klient->kontakt["mesto"] : $O->klient->fa_adresa["mesto"],
				"country" => $O->klient->country->nazov,
				"zip"      => $O->klient->kontakt["psc"] ? $O->klient->kontakt["psc"] : $O->klient->fa_adresa["psc"],
				"ico"      => ( ! $O->klient->fo ) ? $O->klient->fa_ico : "" /*str_pad((string) $O->klient->id, 8, "9", STR_PAD_LEFT)*/,
				"dic"      => $O->klient->fa_dic,
				"icdph"    => $O->klient->fa_icdph,
				"comments" => $O->metadata["poznamka"]
			];
			$db_delivery_address = [
				"name"    => $O->klient_prevadzka->kontakt["meno"] ?$O->klient_prevadzka->kontakt["meno"]: $O->klient_prevadzka->metadata["meno"],
				"surname" => $O->klient_prevadzka->kontakt["priezvisko"] ?$O->klient_prevadzka->kontakt["priezvisko"] : $O->klient_prevadzka->metadata["priezvisko"],
				"firm"    => "",//(!$O->klient_prevadzka->fo)?$O->klient->format_nazov():"", //"J & M & co.",
				"street"  => ($O->klient_prevadzka->kontakt["adresa_ulica"] ?: $O->klient_prevadzka->adresa["adresa_ulica"]) . " " . ($O->klient_prevadzka->kontakt["adresa_cislo"] ?: $O->klient_prevadzka->adresa["adresa_cislo"]),
				"number"  => $O->klient_prevadzka->kontakt["adresa_cislo"] ? $O->klient_prevadzka->kontakt["adresa_cislo"] : $O->klient_prevadzka->adresa["adresa_cislo"],
				"city"    => $O->klient_prevadzka->kontakt["mesto"] ? $O->klient_prevadzka->kontakt["mesto"] : $O->klient_prevadzka->adresa["mesto"],
				"zip"     => $O->klient_prevadzka->kontakt["psc"] ? $O->klient_prevadzka->kontakt["psc"] : $O->klient_prevadzka->adresa["psc"],
				"tel"     => $O->klient_prevadzka->kontakt["telefon"],
				"email"   => $O->klient->kontakt["email"]
			];


			$user = array_map("htmlspecialchars",$db_user);
			$da = array_map("htmlspecialchars",$db_delivery_address);

			/** @var \Logistika $doprava */
			$doprava = $O->logistika;
			/** @var \Platba $platba */
			$platba = $O->platba;


			$number = $O->cislo;
			if($O->export_count > 0){
				$number .= "/".$O->export_count;
			}


			$record = $domtree->createElement("Record");
			$record->setAttribute("IDNum", $O->id);
			$record->setAttribute("Number", $number);
			$record->appendChild($domtree->createElement("Document_Type", "Objednávka"));
			$record->appendChild($domtree->createElement("Document_Number", $number));
			$record->appendChild($domtree->createElement("Date_Document", $O->datum->show("d.m.Y H:i:s")));
			$record->appendChild($domtree->createElement("Notice", $user["comments"]));
			$record->appendChild($domtree->createElement("User_Add", "A"));


			$price = $domtree->createElement("Document_PriceValues");
			$price->appendChild($domtree->createElement("CurrencyCode","EUR"));
			$price->appendChild($domtree->createElement("VAT_Rate_Lower","10"));
			$price->appendChild($domtree->createElement("VAT_Rate_Upper","20"));

			$totalPrice = round($O->suma_spolu(),2);
			if(!$O->isOnlinePayment() && \Kubomikita\Service::getByType(\Kubomikita\Commerce\ShopConfigurator::class)->getParameter("shop","rounding")) {
				$rounding = new \Rounding($totalPrice);
				$totalPrice    = $rounding->math5cent();
			}
			$price->appendChild($domtree->createElement("Price_Total",$totalPrice));

			$record->appendChild($price);

			$company = $domtree->createElement("Company");
			$company->appendChild($domtree->createElement("Name",$this->config->company->name));
			$company->appendChild($domtree->createElement("IdentificationNumber",$this->config->company->identificationNumber));
			$company->appendChild($domtree->createElement("IdentificationNumber_Tax",$this->config->company->identificationNumberTax));
			$company->appendChild($domtree->createElement("IdentificationNumber_VAT",$this->config->company->identificationNumberVAT));
			$company->appendChild($domtree->createElement("EMail",$this->config->company->email));
			$company->appendChild($domtree->createElement("Phone_Number",$this->config->company->phone));
			$company->appendChild($domtree->createElement("Web",$this->config->company->web));

			$caddress = $domtree->createElement("Address_Residence");


			$caddress->appendChild($domtree->createElement("Street_Name_Full",$this->config->company->street));
			$caddress->appendChild($domtree->createElement("PostalCode",$this->config->company->postalCode));
			$caddress->appendChild($domtree->createElement("City",$this->config->company->city));
			$caddress->appendChild($domtree->createElement("Country", $this->config->company->country));
			$company->appendChild($caddress);

			$record->appendChild($company);

			$partner = $domtree->createElement("BusinessPartner");
			$partner->setAttribute("IDNum", $db_user["id"]);
			$partner->appendChild($domtree->createElement("Name", $db_user["firm"]));
			$partner->appendChild($domtree->createElement("IdentificationNumber", $db_user["ico"]));
			$partner->appendChild($domtree->createElement("IdentificationNumber_Tax", $db_user["dic"]));
			$partner->appendChild($domtree->createElement("IdentificationNumber_VAT", $db_user["icdph"]));
			$address = $domtree->createElement("Address_Residence");
			$address->appendChild($domtree->createElement("Street_Name_Full",$db_user["street"]));
			$address->appendChild($domtree->createElement("PostalCode",$db_user["zip"]));
			$address->appendChild($domtree->createElement("City",$db_user["city"]));
			$address->appendChild($domtree->createElement("Country", $db_user["country"]));

			$partner->appendChild($address);

			$record->appendChild($partner);

			$items = $domtree->createElement("Items");
			foreach($O->items() as $Oi) {
				$item = $domtree->createElement( "Item" );
				$item->appendChild($domtree->createElement("Number",htmlspecialchars($Oi->tovar->kod_systemu())  ));
				$item->appendChild($domtree->createElement("Name",htmlspecialchars($Oi->tovar->nazov)  ));
				$item->appendChild($domtree->createElement("Unit","ks" ));

				$item->appendChild( $domtree->createElement( "Amount_Unit", $Oi->mnozstvo_objednane ) );
				$item->appendChild( $domtree->createElement( "Price_WithVAT_Unit", sprintf("%.2F", ($Oi->cena->suma() * $O->zlava_koef()  )) ) );//->setAttribute( "currency","EUR" );
				$item->appendChild( $domtree->createElement( "Price_WithVAT", sprintf("%.2F", ($Oi->cena->multiply($Oi->mnozstvo_objednane)->suma() * $O->zlava_koef()  )) ) );//->setAttribute( "currency","EUR" );
				$item->appendChild($domtree->createElement("VAT_Rate",$Oi->cena->dph()));

				$items->appendChild($item);
			}
			if($doprava->cena instanceof \Cena && $doprava->cena->suma() > 0){
				$item = $domtree->createElement( "Item" );
				$item->appendChild($domtree->createElement("Number", $this->config->kurier_plu  ));
				//$item->appendChild( $domtree->createElement( "oznacenie", $this->config->kurier_plu ) );
				$item->appendChild( $domtree->createElement( "Name",
					htmlspecialchars($doprava->nazov())  )  );
				$item->appendChild($domtree->createElement("Unit","ks" ));
				$item->appendChild( $domtree->createElement( "Amount_Unit", 1 ) );
				$item->appendChild( $domtree->createElement( "Price_WithVAT_Unit", sprintf("%.2F",$doprava->cena->suma()) ) );
				$item->appendChild( $domtree->createElement( "Price_WithVAT", sprintf("%.2F", $doprava->cena->suma()) ) );//->setAttribute( "currency","EUR" );
				$item->appendChild($domtree->createElement("VAT_Rate",$doprava->cena->dph()));
				$items->appendChild( $item );
			}
			if(isset($O->metadata["doprava_prirazka"]) && (float) $O->metadata["doprava_prirazka"] > 0) {
				$item = $domtree->createElement( "Item" );
				$item->appendChild($domtree->createElement("Number", $this->config->dobierka_plu  ));
				//$item->appendChild( $domtree->createElement( "oznacenie", $this->config->dobierka_plu ) );
				$item->appendChild( $domtree->createElement( "Name",
					htmlspecialchars("Dobierka")  )  );
				$item->appendChild($domtree->createElement("Unit","ks" ));
				$item->appendChild( $domtree->createElement( "Amount_Unit", 1 ) );
				$item->appendChild( $domtree->createElement( "Price_WithVAT_Unit", sprintf("%.2F", (float) $O->metadata["doprava_prirazka"]) ) );//->setAttribute( "currency", "EUR" );
				$item->appendChild( $domtree->createElement( "Price_WithVAT", sprintf("%.2F", (float) $O->metadata["doprava_prirazka"]) ) );//->setAttribute( "currency","EUR" );
				$item->appendChild($domtree->createElement("VAT_Rate", $doprava->cena->dph()));
				$items->appendChild( $item );
			}
			if(($dp = $O->zlava()) instanceof \DarcekovaPoukazka){


				$cena = new \Cena(($dp->suma * -1));

				$item = $domtree->createElement( "Item" );
				$item->appendChild($domtree->createElement("Number", $this->config->darcekova_plu  ));
				//$item->appendChild( $domtree->createElement( "oznacenie", $this->config->dobierka_plu ) );
				$item->appendChild( $domtree->createElement( "Name",
					htmlspecialchars("Darčeková poukážka - ".$O->metadata["promo_kod"])  )  );
				$item->appendChild($domtree->createElement("Unit","ks" ));
				$item->appendChild( $domtree->createElement( "Amount_Unit", 1 ) );
				$item->appendChild( $domtree->createElement( "Price_WithVAT_Unit", sprintf("%.2F", (float) $cena->suma()) ) );//->setAttribute( "currency", "EUR" );
				$item->appendChild( $domtree->createElement( "Price_WithVAT", sprintf("%.2F", (float) $cena->suma()) ) );//->setAttribute( "currency","EUR" );
				$item->appendChild($domtree->createElement("VAT_Rate", $cena->dph()));
				//dumpe($dp,$cena,$item);
				$items->appendChild( $item );
			}

			$record->appendChild($items);


			$list->appendChild($record);

			//if($O->export_count > 0){
				//$id .= substr($chars,((int)$O->export_count-1),1);
			//	$id.=$O->export_count;
			//}

			/*$eshopOrder = $domtree->createElement( "objednavka" );
			$eshopOrder = $xmlRoot->appendChild( $eshopOrder );

			$eshopOrder->appendChild($domtree->createElement("typ","OP"));
			$eshopOrder->appendChild($domtree->createElement("stavobj","EV"));
			$eshopOrder->appendChild($domtree->createElement("cislo", $O->cislo));
			$eshopOrder->appendChild($domtree->createElement("datum", $O->datum->show("d.m.Y")));
			$eshopOrder->appendChild($domtree->createElement("typceny", 2));
			//$eshopOrder->appendChild($domtree->createElement("vydajkatyp", "VPF"));
			$eshopOrder->appendChild($domtree->createElement("poznamka", Strings::substring($O->metadata["poznamka"],0,20)));
			$eshopOrder->appendChild($domtree->createElement("sposobdopravy",  $doprava->nazov()));
			$eshopOrder->appendChild($domtree->createElement("sposobuhrady", $platba->nazov()));
			if($O->klient_prevadzka->id > 0) {
				$eshopOrder->appendChild( $domtree->createElement( "firmapostadresa", "A" ) );
			}

			$person = $domtree->createElement( "firma" );
			$person = $eshopOrder->appendChild( $person );
			$person->appendChild($domtree->createElement("id",$user["id"]));
			$person->appendChild($domtree->createElement("ico",$user["ico"]));
			$person->appendChild($domtree->createElement("dic",$user["dic"]));
			$person->appendChild($domtree->createElement("icdph",$user["icdph"]));
			$person->appendChild($domtree->createElement("nazov1",$user["firm"]));
			$person->appendChild( $domtree->createElement( "ulica", $user["street"] ) );
			$person->appendChild( $domtree->createElement( "obec", $user["city"] ) );
			$person->appendChild( $domtree->createElement( "psc", $user["zip"] ) );
			$person->appendChild( $domtree->createElement( "mobil", $da["tel"] ) );
			$person->appendChild( $domtree->createElement( "email", $da["email"] ) );

			if($O->klient_prevadzka->id > 0){
				$person->appendChild($domtree->createElement("postnazov1",$da["name"]." ".$da["surname"]));
				$person->appendChild($domtree->createElement("postulica",$da["street"]));
				$person->appendChild($domtree->createElement("postobec",$da["city"]));
				$person->appendChild($domtree->createElement("postpsc",$da["zip"]));
			}

			foreach($O->items() as $Oi) {
				$item = $domtree->createElement( "pohyb" );
				$item->appendChild( $domtree->createElement( "oznacenie", $Oi->tovar->kod_systemu ) );
				$item->appendChild( $domtree->createElement( "nazov",
					htmlspecialchars($Oi->tovar->nazov)  )  );
				$item->appendChild( $domtree->createElement( "pocet", $Oi->mnozstvo_objednane ) );
				$item->appendChild( $domtree->createElement( "cena", sprintf("%.2F", ($Oi->cena->suma() * $O->zlava_koef()  )) ) );//->setAttribute( "currency","EUR" );
				$item->appendChild($domtree->createElement("pdph",$Oi->cena->dph()));
				$eshopOrder->appendChild( $item );
			}
			if($doprava->cena instanceof \Cena && $doprava->cena->suma() > 0){
				$item = $domtree->createElement( "pohyb" );
				$item->appendChild( $domtree->createElement( "oznacenie", $this->config->kurier_plu ) );
				$item->appendChild( $domtree->createElement( "nazov",
					htmlspecialchars($doprava->nazov())  )  );
				$item->appendChild( $domtree->createElement( "pocet", 1 ) );
				$item->appendChild( $domtree->createElement( "cena", sprintf("%.2F",$doprava->cena->suma()) ) );
				$item->appendChild($domtree->createElement("pdph",$doprava->cena->dph()));
				$eshopOrder->appendChild( $item );
			}
			if(isset($O->metadata["doprava_prirazka"]) && (float) $O->metadata["doprava_prirazka"] > 0) {
				$item = $domtree->createElement( "pohyb" );
				$item->appendChild( $domtree->createElement( "oznacenie", $this->config->dobierka_plu ) );
				$item->appendChild( $domtree->createElement( "nazov",
					htmlspecialchars("Dobierka")  )  );
				$item->appendChild( $domtree->createElement( "pocet", 1 ) );
				$item->appendChild( $domtree->createElement( "cena", sprintf("%.2F", (float) $O->metadata["doprava_prirazka"]) ) );//->setAttribute( "currency", "EUR" );
				$item->appendChild($domtree->createElement("pdph", $doprava->cena->dph()));
				$eshopOrder->appendChild( $item );
			}*/

			$ids[] = (int) $O->id;
		}

		if($oberonRequest and !empty($ids)){
			$this->db->query("UPDATE ec_objednavky SET",["export" => 1],"WHERE",["id" => $ids]);
			\Cache::flush("ec_objednavky");
			\Cache::flush("ec_objednavky_stavy");
			\Cache::flush("ec_objednavky_search");

			\Cache::flush("objednavky");
			\Cache::flush("objednavky_stavy");
			\Cache::flush("objednavky_search");
		}



		$domtree->formatOutput = true;

		$xml = $domtree->saveXML();
		$this->sendXmlResponse($xml);
		//dump($xml);
	}

	/**
	 * @method POST
	 * @param int $partialUpdate
	 */
	public function actionProducts(int $partialUpdate) : void
	{

	}

	/**
	 * @method POST
	 * @param string $filename
	 */
	public function actionImages(string $filename) : void {


	}

	private function loadCategories($file){

	}

	private function loadProducts(string $file, $partial = false){



	}
}