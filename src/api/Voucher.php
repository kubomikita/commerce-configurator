<?php


namespace Kubomikita\Api;


use Kubomikita\Commerce\ProteinConfigurator;
use Kubomikita\Utils\Arrays;
use Nette\Application\BadRequestException;
use Nette\InvalidStateException;
use Nette\Utils\ArrayHash;
use Nette\Utils\Strings;

class Voucher extends BaseApi {
	public $types =array(
		"small"=>array(
			"image"=>WWW_DIR."images/poukazka_small.jpg",
			"fontsizeValue" => 23,
			"fontsizeCode" => 22,
			"valueLeft" => 210,
			"valueTop" => 290,
			"codeLeft" => 270,
			"codeTop" => 390,
			"codeColor" => "FFFFFF",
			"valueColor" => "000000",
		),
		"full"=>array(
			"image"=>WWW_DIR."images/poukazka_full.jpg",
			"fontsizeValue" => 51,
			"fontsizeCode" => 49,
			"valueLeft" => 330,
			"valueTop" => 720,
			"codeLeft" => 470,
			"codeTop" => 875,
			"codeColor" => "FFFFFF",
			"valueColor" => "000000",
		)
	);

	public function beforeStartup() {
		// TODO: Implement beforeStartup() method.
	}

	public function actionGet(string $type, string $code, int $value){

		//dumpe($this->parameters->shop->darcekovaPoukazka);

		$types = \Nette\Utils\Arrays::mergeTree(Arrays::objectToArray($this->parameters->shop->darcekovaPoukazka ?? new ArrayHash()), $this->types);
		//dumpe($types);
		if((isset($types["full"]["primary"]) && $types["full"]["primary"])) {
			$type = 'full';
		} elseif((isset($types["small"]["primary"])  && $types["small"]["primary"])){
			$type = 'small';
		}
		//dumpe($types,  Arrays::objectToArray($this->parameters->shop->darcekovaPoukazka), );
		if($this->types[$type]["image"] != null){
			$rImg = ImageCreateFromJPEG($types[$type]["image"]);

			$fontsizeValue = $types[$type]["fontsizeValue"];
			$fontsizeCode = $types[$type]["fontsizeCode"];

			$valueLeft = $types[$type]["valueLeft"];
			$valueTop = $types[$type]["valueTop"];

			$codeLeft = $types[$type]["codeLeft"];
			$codeTop = $types[$type]["codeTop"];

			list($r, $g, $b) = array_map('hexdec', str_split($types[$type]["valueColor"], 2));
			$valueColor = imagecolorallocate($rImg, $r, $g, $b);
			list($r, $g, $b) = array_map('hexdec', str_split($types[$type]["codeColor"], 2));
			$codeColor = imagecolorallocate($rImg, $r, $g, $b);
			$font = isset($types[$type]["font"]) ? $types[$type]["font"] : WWW_DIR.'assets/fonts/opensans/OpenSans-Regular.ttf';
			if(!file_exists($font)){
				throw new InvalidStateException("Font in '$font' is missing.");
			}

			if(isset($types[$type]["text"])){
				$hodnota = sprintf($types[$type]["text"], $value);
			} else {
				$hodnota = "na nákup v hodnote ". $value." €";
			}

			$kod = urldecode($code);

			imagettftext($rImg, $fontsizeValue, 0, $valueLeft, $valueTop, $valueColor, $font, $hodnota);
			imagettftext($rImg, $fontsizeCode, 0, $codeLeft, $codeTop, $codeColor, $font, $kod);
			header('Content-type: image/jpeg');
			imagejpeg($rImg,NULL,100);
			exit;
		}
	}

}
