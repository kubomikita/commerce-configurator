<?php


namespace Kubomikita\Api;


use Kubomikita\Commerce\ProteinConfigurator;


class Cache extends BaseApi{
	public function beforeStartup() {
		// TODO: Implement beforeStartup() method.
	}

	public function actionOpcache(int $flush = 0){
		if($flush){
			opcache_reset();
		}
		$status = opcache_get_status();
		//dump($this, opcache_get_configuration(),opcache_get_status(),opcache_reset(), opcache_get_status());
		$this->sendJsonResponse([
			"result" => [
				"enabled" => $status["opcache_enabled"],
				"revalidateFreq" => (int) ini_get("opcache.revalidate_freq"),
				"restartPending" => $status["restart_pending"],
				"manualRestarts" => $status["opcache_statistics"]["manual_restarts"],
				"memory" => $status["memory_usage"],
				"jit" => $status["jit"]
			]
		]);
	}

	public function actionFlush(){
		if(!$this->context->isDebugMode()){
			echo "Access rejected!";
			exit;
		}
		$dir = $this->context->getCacheDirectory()."/";
		$file = "*";
		if(isset($this->args["file"])) { $file = $this->args["file"];}

		foreach(glob($dir.$file) as $f){
			if(is_dir($f)){
				foreach(glob($dir.$file."/*") as $ff){
					if(is_dir($ff)){
						foreach(glob($ff."/*") as $fff){
							@unlink($fff);
							echo $fff.'<br>'.PHP_EOL;
						}
						rmdir($ff);
						echo $f.' - <span style="color:red">cely priecinok vymazany.</span><br>'.PHP_EOL;
					} else {
						@unlink($ff);
						echo $ff.'<br>'.PHP_EOL;
					}

				}
				rmdir($f);
				echo $f.' - <span style="color:red">cely priecinok vymazany.</span><br>'.PHP_EOL;
				//unlink($f);
			} else {
				echo $f.'<br>'.PHP_EOL;
				@unlink($f);
			}
		}
	}
}