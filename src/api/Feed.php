<?php


namespace Kubomikita\Api;


use Commerce\Xml\XmlFeed;
use Kubomikita\Commerce\ProteinConfigurator;
use Nette\Application\BadRequestException;
use Nette\Application\Routers\Route;
use Nette\InvalidStateException;
use Nette\Utils\Strings;

class Feed extends BaseApi{

	public function beforeStartup() {	}

	public function actionShow(string $name){

		$filename = $this->args["name"];
		$e = explode(".",$filename);
		$ext = Strings::firstUpper(end($e));

		$class = "\\Commerce\\".$ext."\\".Route::path2presenter($e[0]);
		//dumpe($e,$class);
		if(!class_exists($class)) {
			trigger_error("Feed ".$class." not exists");
			throw new BadRequestException("Feed not exists");
		}
		/** @var $feed XmlFeed */
		$feed = new $class();
		$output = null;

		try {
			ob_start();
			echo $feed->render();
			$output = ob_get_clean();
			if($output !== null){
				$feed->headers();
				echo $output;
			}
		} catch (InvalidStateException $e){
			echo $e->getMessage();
			dump($e);
		}


	}

}