<?php


namespace Kubomikita\Api;


use Exception;
use Kubomikita\Service;
use Nette\Application\AbortException;
use Nette\Application\BadRequestException;
use Nette\Application\ForbiddenRequestException;
use Nette\Application\Responses\FileResponse;
use Nette\Database\Connection;
use Nette\Database\ResultSet;
use Nette\InvalidArgumentException;
use Nette\Utils\DateTime;
use Nette\Utils\FileSystem;
use Nette\Utils\Image;
use Nette\Utils\Json;
use Nette\Utils\Random;
use Nette\Utils\Strings;
use Prewk\XmlStringStreamer;
use Prewk\XmlStringStreamer\Parser\StringWalker;
use Prewk\XmlStringStreamer\Parser\UniqueNode;
use Prewk\XmlStringStreamer\Stream\File;
use Tracy\Debugger;

class Mailerlite extends BaseApi{

	protected $logRequests = true;

	protected $secret;

	public function beforeStartup()
	{
		Debugger::$showBar = false;
		$this->secret = 'iZrEiq6suI';
	}

	/**
	 * @method POST
	 */
	public function actionGenerateVoucher(): void
	{
		$this->checkSignature();
		$content = Json::decode($this->request->getRawBody(), Json::FORCE_ARRAY);
		//$content = $content['subscriber'];

		if ($content['type'] !== 'subscriber.added_to_group') {
			$this->sendJsonResponse(['state' => 'bad event']);
		}

		if ($content['group']['id'] !== "121393884014053303") {
			$this->sendJsonResponse(['state' => 'bad group id']);
		}

		$email = /*$content['email']*/  $content['subscriber']['email'] ?? null;

		if ($email === null) {
			$this->sendJsonResponse(['state' => 'bad data']);
		}


		$apiKey = $this->parameters['mailerlite']['apiKey'];

		$mailerlite = new \MailerLite\MailerLite(['api_key' => $apiKey]);

		//$mailerlite->webhooks->update(125932809501017197, ['url' =>'https://www.protein.sk/api/mailerlite/generate-voucher', 'events' => ['subscriber.created', 'subscriber.added_to_group']]);
		//$mailerlite->webhooks->delete(125929237632779659);
		//$mailerlite->webhooks->create(['url' =>'https://www.protein.sk/api/mailer-lite/generate-voucher', 'events' => ['subscriber.created']]);
		//dumpe(/*,*/ /*$mailerlite->webhooks->create(['url' =>'https://www.protein.sk/api/mailer-lite/generate-voucher', 'events' => ['subscriber.added_to_group']])*/);

		try {


			$subscriber = $mailerlite->subscribers->find($email)['body']['data'];

			$dp = new \PromoKod();
			$dp->skratka = 'YNK';
			$dp->zlava = 5;
			$dp->platnost_od = Timestamp();
			$dp->platnost_do = Timestamp()->shift('1y');
			$dp->kod = Random::generate(5,'0-9A-Z').'-'.Random::generate(8,'0-9A-Z');
			$dp->aktivny = 1;
			$dp->reuse = 0;
			$dp->popis = $email;
			$dp->save();

			$mailerlite->subscribers->update($subscriber['id'], ['fields' => ['first_voucher' => $dp->kod]]);

		} catch (\Throwable $e) {
			$this->logRequest($this->request, new BadRequestException('Subscriber email '.$email.' not found'));
			$this->response->setCode(404);
			$this->sendJsonResponse(['state' => 'not found']);
		}

		$webhooks = $mailerlite->webhooks->get();

		$this->sendJsonResponse(['state' => 'ok','data' => ['email' => $email/*, 'voucherCode' => $dp->kod ?? null*/], 'request' => $content,'subscriber' => $subscriber ?? [], 'webhooks' => $webhooks['body']['data']/*, 'signature' => $this->generateSignature($content, $this->secret)*/]);
	}

	private function checkSignature(): void
	{
		$signatureRequest = $this->request->getHeader('Signature');
		$signature = $this->generateSignature($this->request->getRawBody(), $this->secret);

		if($signature !== $signatureRequest) {
			$this->logRequest($this->request, new ForbiddenRequestException('Signature is not valid.'));
			$this->response->setCode(401);
			$this->sendJsonResponse(['state' => 'unauthorized']);
		}
	}

	private function generateSignature(string $jsonPayload, string $secret): string
	{
		return hash_hmac('sha256', $jsonPayload, $secret);
	}
}