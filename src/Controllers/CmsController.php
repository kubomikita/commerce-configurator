<?php

abstract class CmsController extends CommerceController {
	public static function getKey(): string {
		$c    = parent::getControllerName();
		$keys = splitAtUpperCase( $c );
		$key  = strtolower( implode( "_", $keys ) );

		return $key;
	}
}