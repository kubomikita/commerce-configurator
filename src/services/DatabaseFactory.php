<?php


namespace Kubomikita\Commerce;

use Nette\Database;
use Nette\Caching;

class DatabaseService implements IService{
	/** @var Caching\IStorage */
	public static $cacheStorage;
	/**
	 * @param ProteinConfigurator $configurator
	 *
	 * @return array
	 */
	public function getNetteDatabase($configurator){
		$storage = new Caching\Storages\FileStorage($configurator->getCacheDirectory());

		$db = $configurator->getParameter("database");
		if($db["socket"] != ""){
			$dsn = "mysql:unix_socket=".$db["socket"].";dbname=".$db["name"];
		} else {
			$dsn = "mysql:host=".$db["host"].";dbname=".$db["name"];
		}

		$database = new Database\Connection($dsn,$db["user"],$db["password"], ["charset" => "utf8"]);
		$db_structure = new Database\Structure($database,$storage);
		$db_conventions = new Database\Conventions\DiscoveredConventions($db_structure);
		$db_context = new Database\Context($database,$db_structure,$db_conventions,$storage);

		return ["database"=>$database,"database.context"=>$db_context];
	}

	public function startup($configurator){
		\CommerceDB::connect($configurator);
		return $this->getNetteDatabase($configurator);
	}


	public static function diConnectionStartup(IConfigurator $configurator): Database\Connection
	{
		$db = $configurator->getParameter("database");
		if($db["socket"] != ""){
			$dsn = "mysql:unix_socket=".$db["socket"].";dbname=".$db["name"];
		} else {
			$dsn = "mysql:host=".$db["host"].";dbname=".$db["name"];
		}
		$database = new Database\Connection($dsn,$db["user"],$db["password"], ["charset" => "utf8"]);
		self::$cacheStorage = new Caching\Storages\FileStorage($configurator->getCacheDirectory());

		\CommerceDB::connect($configurator);

		return $database;
	}

	public static function disconnect(Database\Connection $connection){
		$connection->disconnect();
		mysqli_close(\CommerceDB::$DB);
		//bdump($connection,"DATABASE disconnect");
	}

	public static function diContextStartup(Database\Connection $connection): Database\Context
	{
		$storage = self::$cacheStorage;

		$db_structure = new Database\Structure($connection,$storage);
		$db_conventions = new Database\Conventions\DiscoveredConventions($db_structure);
		$db_context = new Database\Context($connection,$db_structure,$db_conventions,$storage);

		return $db_context;
	}
}