<?php

namespace Kubomikita\Commerce;

use Kubomikita\Form;
use Kubomikita\FormFactory;
use Kubomikita\IFromAjaxHandler;
use Nette\DI\Container;
use Nette\Http\Request;
use Nette\Utils\Callback;

class RouterFactory {
	/**
	 * @var Container
	 */
	private $container;
	/**
	 * @var IDiConfigurator
	 */
	private $conf;
	/**
	 * @var array
	 */
	private $initialized = [];
	private $routers = [];

	/**
	 * RouterFactory constructor.
	 *
	 * @param IDiConfigurator $container
	 */
	public function __construct(IDiConfigurator $container) {
		$this->conf = $container;
		$this->container = $container->container;
		$this->routers = $this->conf->getRouters();

	}
	public function getByType($class){
		return [$this,array_search($class,$this->routers)]($class);
	}
	public function getRouter($class){
		$this->initialized[] = $class;
		return [$class,"create"]($this->container->getByType(Container::class));
	}
	public function api($class){
		$conf = $this->conf;
		$request = $this->container->getByType(Request::class);
		if(!$conf->isCli()) {
			if ( $request->getQuery( 'api' ) ) {
				return $this->getRouter($class);
			}
		}
	}
	public function ajax($class){
		$request = $this->container->getByType(Request::class);
		if(!$this->conf->isCli()) {
			if ( $request->isAjax() ) {
				/*$formFactory = $this->container->getByType(FormFactory::class);
				if($formFactory->getAjaxHandler() instanceof IFromAjaxHandler){
					dump($this,Form::$instances);
					exit;
				}*/
				$this->getRouter($class);
			}
		}
	}
	public function cli($class){
		$conf = $this->conf;
		if ( $conf->isCli() ) {
			return $this->getRouter($class);
		}
	}
	public function redirects($class){
		if(!$this->conf->isCli()){
			return $this->getRouter($class);
		}
	}

}