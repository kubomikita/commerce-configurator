<?php


namespace Kubomikita\Commerce;

use Deployment\Logger;
use Tracy,
	Nette;

class TracyPanelService implements IService{

	public function startup($configurator){
		if(isset($configurator->container["user"]) and $configurator->container["user"] instanceof Nette\Security\User) {
			Tracy\Debugger::getBar()->addPanel( new Nette\Bridges\SecurityTracy\UserPanel( $configurator->container["user"] ) );
			Tracy\Debugger::getBar()->addPanel( new Nette\Bridges\HttpTracy\SessionPanel );
			$databasePanel = new Nette\Bridges\DatabaseTracy\ConnectionPanel( $configurator->container["database"]);
			$databasePanel->maxQueries=1000;
			Tracy\Debugger::getBar()->addPanel( $databasePanel );
			//
		}
	}
	public function afterLoad($configurator){
		if(isset($configurator->container["microdata"])) {
			Tracy\Debugger::getBar()->addPanel( new MicrodataPanel( $configurator->container["microdata"] ) );
		}
		if(isset($configurator->container["mail"])){
			Tracy\Debugger::getBar()->addPanel( new MailPanel($configurator->container["mail"]));
		}
	}
	public function diStartup(ShopConfigurator $configurator){
		if($configurator->container->hasService("user")/*isset($configurator->container["user"]) and $configurator->container["user"] instanceof Nette\Security\User*/) {
			Tracy\Debugger::getBar()->addPanel( new Nette\Bridges\SecurityTracy\UserPanel( $configurator->container->getService("user") ) );
		}
		Tracy\Debugger::getBar()->addPanel( new Nette\Bridges\HttpTracy\SessionPanel );
		$databasePanel = new Nette\Bridges\DatabaseTracy\ConnectionPanel( $configurator->container->getService("database"), Tracy\Debugger::getBlueScreen());
		$databasePanel->maxQueries=1000;
		Tracy\Debugger::getBar()->addPanel( $databasePanel );
	}
	public function diAfterLoad(ShopConfigurator $configurator){
		if($configurator->container->hasService("microdata") && $configurator->container->isCreated("microdata")) {
			Tracy\Debugger::getBar()->addPanel( new MicrodataPanel( $configurator->container->getService("microdata")));
		}
		if($configurator->container->hasService("mail")/* && $configurator->container->isCreated("mail")*/){
			Tracy\Debugger::getBar()->addPanel( new MailPanel($configurator->container->getService("mail")));
		}
		if($configurator->container->hasService("dataLayerContainer")){
			Tracy\Debugger::getBar()->addPanel( new DataLayerPanel($configurator->container->getService("dataLayerContainer")));
		}
	}
}