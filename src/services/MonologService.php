<?php

namespace Kubomikita\Commerce;


use Kdyby\Monolog\Logger;
use Kdyby\Monolog\Processor\TracyExceptionProcessor;
use Kdyby\Monolog\Processor\TracyUrlProcessor;
use Kdyby\Monolog\Tracy\BlueScreenRenderer;
use Kdyby\Monolog\Tracy\MonologAdapter;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\SyslogHandler;
use Monolog\Processor\IntrospectionProcessor;
use Monolog\Processor\MemoryPeakUsageProcessor;
use Monolog\Processor\WebProcessor;
use Nette;
use Nette\Http\Request;
use Nette\DI\Container;
use Tracy\BlueScreen;

class MonologService implements IService {


	public function startup( $configurator ) {

	}

	/**
	 * @param Container $container
	 *
	 * @return Logger
	 */
	public static function diStartup( Container $container ) {
		$kdyby = new Logger($container->parameters["shop"]["name"]);
		$kdyby->pushHandler(new StreamHandler($container->parameters["logDir"]."/error.log"));
		$kdyby->pushHandler(new SyslogHandler($container->parameters["shop"]["name"]));
		$kdyby->pushProcessor(new TracyExceptionProcessor($container->getByType(BlueScreenRenderer::class)));
		$kdyby->pushProcessor(new WebProcessor($_SERVER));
		//$kdyby->pushProcessor(new IntrospectionProcessor());
		$kdyby->pushProcessor(new MemoryPeakUsageProcessor());
		return $kdyby;
	}

	/**
	 * @param Container $container
	 *
	 * @return MonologAdapter
	 */
	public static function getAdapter(Container $container){
		$adapter = new MonologAdapter($container->getByType(Logger::class),$container->getByType(BlueScreenRenderer::class),$container->parameters["adminEmail"]);
		return $adapter;
	}

	/**
	 * @param Container $container
	 *
	 * @return BlueScreenRenderer
	 */
	public static function getBluescreenRenderer(Container $container){
		$br = new BlueScreenRenderer($container->parameters["logDir"],new BlueScreen());
		return $br;
	}

}