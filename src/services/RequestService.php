<?php

namespace Kubomikita\Commerce;


use Nette;
use Nette\Http\Request;
use Nette\DI\Container;
class RequestService implements IService{
	/**
	 *
	 * @param Container $configurator
	 *
	 * @return Request
	 */
	public function startup($configurator){
		$ip = (isset($_SERVER["HTTP_X_REAL_IP"]))?$_SERVER["HTTP_X_REAL_IP"]:$_SERVER["REMOTE_ADDR"];
		return new Nette\Http\Request(\Registry::get("url"),null,$_POST,$_FILES,$_COOKIE,apache_request_headers(),null,$ip);
	}

	public static function diStartup(Container $container):Request
	{
		if(!(PHP_SAPI === 'cli')) {
			$ip = ( isset( $_SERVER["HTTP_X_REAL_IP"] ) ) ? $_SERVER["HTTP_X_REAL_IP"] : $_SERVER["REMOTE_ADDR"];

			$requestFactory = new Nette\Http\RequestFactory();
			$request = $requestFactory->fromGlobals();
			$url = $request->getUrl()->withQuery($_REQUEST);
			return $requestFactory->fromGlobals()->withUrl($url);
		}
		return new Request(new Nette\Http\UrlScript());
	}
}