<?php


namespace Kubomikita\Commerce;

use Kubomikita\DefaultFormTranslator;
use Kubomikita\FormAlertHandler;
use Nette;
use Kubomikita\FormFactory,
	Kubomikita\FormAjaxHandler;

class KubomikitaFormService implements IService{
	public function startup($container){
		if(!$container->isCli()) {
			$formfactory = new FormFactory();
			$formfactory->setAjaxHandler( new FormAjaxHandler( $_GET, $_POST, $_FILES ) );
			$formfactory->setAlertHandler(new FormAlertHandler());
			$formfactory->setJavascriptFilename("assets/js/formfactory.min.js");
			$formfactory->setWithJquery(false);
			$formfactory->register();

			return $formfactory;
		}
	}


	public static function diStartup(IConfigurator $container): FormFactory
	{
		if(!$container->isCli()) {

			$formfactory = new FormFactory();
			$formfactory->setAjaxHandler( new FormAjaxHandler( $_GET, $_POST, $_FILES ) );
			$formfactory->setAlertHandler(new FormAlertHandler());
			$formfactory->setJavascriptFilename("assets/js/formfactory.min.js");
			$formfactory->setWithJquery(false);
			$formfactory->setTranslator(new DefaultFormTranslator());
			$formfactory->setUseSessions(false);
			//$formfactory->listen();
			//$formfactory->register();

			return $formfactory;
		}
		return new FormFactory();
	}
}