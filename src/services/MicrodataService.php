<?php


namespace Kubomikita\Commerce;


class MicrodataService implements IService{

	public function startup($configurator){
		$microdata = new Microdata($configurator);
		return $microdata;
	}

	public static function diStartup(IConfigurator $configurator):Microdata
	{
		$microdata = new Microdata($configurator);
		return $microdata;
	}
}