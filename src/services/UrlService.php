<?php


namespace Kubomikita\Commerce;

use Nette;
use Nette\DI\Container;
use Nette\Http\UrlScript;

class UrlService implements IService{

	/**
	 * @param $configurator
	 *
	 * @return Nette\Http\UrlScript
	 */
	public function startup($configurator){
		$url = new UrlScript(self::getRequestScheme()."://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"]);
		$url->setQuery($_GET);
		return $url;
	}

	public static function diStartup(Container $container): UrlScript
	{
		$url = new UrlScript(self::getRequestScheme()."://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"]);
		//$url->setQuery($_GET);
		return $url;
	}

	private static function getRequestScheme(){
		if(isset($_SERVER['HTTP_X_FORWARDED_PROTO'])){
			return $_SERVER['HTTP_X_FORWARDED_PROTO'];
		} else {
			if(isset($_SERVER['HTTP_HTTPS']) == "on"){
				return "https";
			}
		}
		return "http";
	}
}