<?php
namespace Kubomikita\Commerce;

use Kdyby\Redis\RedisClient;
use Kdyby\Redis\RedisStorage;
use Nette\Caching\Cache;
use Nette\Caching\IStorage;
use Nette\Caching\Storages\DevNullStorage;
use Nette\Caching\Storages\FileStorage;
use Nette\Caching\Storages\SQLiteJournal;
use Nette\Utils\FileSystem;
use Nette\Utils\Finder;

class CacheService implements IService {

	/**
	 * CacheService constructor.
	 *
	 * @param IDiConfigurator $configurator
	 * @return IStorage
	 */
	public function __construct(IDiConfigurator $configurator) {
		$cache = new FileStorage($configurator->getCacheDirectory());
		return $cache;
	}

	public function startup( $configurator ) {
		// TODO: Implement startup() method.
	}

	public static function diStartup(IDiConfigurator $configurator ) : IStorage
	{
		try {
			$journal = new SQLiteJournal( $configurator->getCacheDirectory() . "/journal.s3db" );
		} catch (\Throwable $e){
			$journal = null;
		}

		return new FileStorage($configurator->getCacheDirectory(), $journal);
	}

	public static function diStartupCZ(IDiConfigurator $configurator): IStorage
	{
		$dir = $configurator->getCacheDirectory() . "/cz";
		if(!file_exists($dir)){
			FileSystem::createDir($dir);
		}
		return new FileStorage($dir);
	}
}