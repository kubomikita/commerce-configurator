<?php
namespace Kubomikita\Commerce;

use Nette\Utils\Finder;

class ControllerService implements IService {
	/** @var IConfigurator */
	protected $configurator;
	protected $dir;
	/** @var array Of controller */
	protected $controllers = [];
	protected $controllersByName = [];
	/**
	 * ControllerService constructor.
	 *
	 * @param IConfigurator $configurator
	 * @param Microdata $microdata
	 */
	public function __construct(IConfigurator $configurator, $dir = APP_DIR."controller") {
		$this->configurator = $configurator;
		$this->dir = $dir;
	}

	/**
	 * Creates array of all controllers
	 * @return array
	 */
	public function create(){
		/** @var $Controller \SplFileInfo $file */
		foreach(Finder::find("*_controller.php")->in($this->dir) as $Controller){
			$className = str_replace(".php","",$Controller->getFilename());
			$key = $className::getKey();
			if(!isset($this->controllers[$key])){
				$this->controllers[$key] = new $className($this->configurator);
				$this->controllersByName[$className] = $this->controllers[$key];
			}
		}
		return $this->controllers;
	}

	/**
	 * Return Controller by name
	 * @param string $controller
	 *
	 * @return \CommerceController
	 */
	public function createOne($controller){
		if(!isset($this->controllersByName[$controller])) {
			return new $controller($this->configurator,$this->dir);
		}
		return $this->controllersByName[$controller];
	}

	/**
	 * @deprecated
	 * @param $configurator
	 */
	public function startup( $configurator ) {

	}


}