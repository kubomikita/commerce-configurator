<?php
namespace Kubomikita\Commerce;

use Nette\Application\AbortException;
use Nette\DI\Container;
use Nette\Http\Request;
use Nette\Http\Session;
use Nette\Http\SessionSection;
use Nette\Mail\IMailer;

class MailService implements IService{

	const SESSION_SECTION = "MailMessages";

	/** @var IMailer */
	public $mailer;
	/** @var ProteinConfigurator */
	public $context;
	/** @var SessionSection */
	public $session;
	public $options = [];
	/** @var Nette\Mail\Message[]  */
	//public $messages = [];

	/**
	 * @param Configurator $configurator
	 */
	public function startup( $configurator ) {
		$this->context = $configurator;
		/** @var Session $session */
		$session =$this->context->getService("session");
		$this->session = $session->getSection("MailMessages");
		$this->session->setExpiration(0);
		$config = $configurator->getParameter("mail");

		if(!is_array($this->session->messages)){
			$this->session->messages = [];
		}

		if(isset($config["mailer"])){
			$class = $config["mailer"];
			unset($config["mailer"]);
			$this->options = $options =  $config;
			$this->mailer = new $class($options);
		} else {
			echo "Mailer is not defined.";
			throw new AbortException("mailer is not defined");
		}

		return $this;
	}
	public function __construct(Container $container = null) {
		if($container!==null) {
			$session       = $container->getService( "session" );
			$configurator  = $container->getService( "configurator" );
			$this->session = $session->getSection( self::SESSION_SECTION );
			$this->session->setExpiration( 0 );
			$config = $configurator->getParameter( "mail" );
			if ( ! is_array( $this->session->messages ) ) {
				$this->session->messages = [];
			}
			if ( isset( $config["mailer"] ) ) {
				$class = $config["mailer"];
				unset( $config["mailer"] );
				$this->options = $options = $config;
				$this->mailer  = new $class( $options );
			} else {
				echo "Mailer is not defined.";
				throw new AbortException( "mailer is not defined" );
			}
		}
	}

	public function setMail(array $mail){
		$this->session->messages[] = $mail;
	}
	public function getMails(){
		return $this->session->messages;
	}
	public function flushMails(){
		$this->session->messages = [];
	}
}