<?php

namespace Kubomikita\Commerce;

use Nette\Application\AbortException;
use Nette\Utils\Html;

class EcommerceTracking {
	/** @var \Objednavka */
	private $order;
	private $prefix = "transaction";
	private $action = "dataLayer.push";
	private $output = [];

	public function __construct(\Objednavka $O) {
		if(!($O->id > 0)){
			throw new AbortException("Order not exists.");
		}
		$this->order = $O;
	}

	private function dataLayerInit(){
		return 'window.dataLayer = window.dataLayer || [];'."\n";
	}
	public function setPrefix($prefix = null){
		$this->prefix = $prefix;
		return $this;
	}
	public function setAction($action){
		$this->action = $action;
		return $this;
	}
	public function getOutput(){
		return $this->output;
	}

	/**
	 * Outputs Tracking code
	 * @return string
	 */
	public function track(){
		$suma_spolu = \Format::money_user_plain($this->order->suma_spolu_bez_dopravy()/1.2);
		$suma_dph = \Format::money_user_plain($this->order->suma_spolu_bez_dopravy()-$suma_spolu);
		$doprava = \Format::money_user_plain($this->order->logistika->cena->suma());

		$this->output[$this->prefix."Id"] = $this->order->cislo;
		$this->output[$this->prefix."Affiliation"] = \Registry::get("container")->getParameter("shop","name");
		$this->output[$this->prefix."Total"] = $suma_spolu;
		$this->output[$this->prefix."Tax"] = $suma_dph;
		$this->output[$this->prefix."Shipping"] = $doprava;
		/** @var \ObjednavkaItem $oi */
		foreach($this->order->items() as $oi) {
			$cena = $oi->cena->multiply($this->order->zlava_koef())->zaklad();
			$this->output[ $this->prefix . "Products" ][] = [
				"sku" => $oi->tovar->id,
				"name" => (string) $oi->tovar->nazov,
				"category" => (string) $oi->tovar->primarna_kategoria->nazov,
				"price" => \Format::money_user_plain($cena),
				"quantity" => $oi->mnozstvo_objednane
			];
		}

		$html = $this->dataLayerInit();
		$html .= $this->action."(".static::arrayToJsObject($this->output).");";
		$script = Html::el("script")->setAttribute("type","text/javascript")->setHtml($html);
		bdump($this->getOutput(),"EcommerceTracking");
		return $script->render();
	}
	private static function arrayToJsObject(array $arr, $sequential_keys = false, $quotes = true, $beautiful_json = false) {

		$output = static::isAssoc($arr) ? "{" : "[";
		$count = 0;
		foreach ($arr as $key => $value) {

			if ( static::isAssoc($arr) || (!static::isAssoc($arr) && $sequential_keys == true ) ) {
				$output .= ($quotes ? '"' : '') . $key . ($quotes ? '"' : '') . ' : ';
			}

			if (is_array($value)) {
				$output .= static::arrayToJsObject($value, $sequential_keys, $quotes, $beautiful_json);
			} else if (is_bool($value)) {
				$output .= ($value ? 'true' : 'false');
			} else if (is_numeric($value)) {
				$output .= $value;
			} else {
				$output .= ($quotes || $beautiful_json ? '"' : '') . $value . ($quotes || $beautiful_json ? '"' : '');
			}

			if (++$count < count($arr)) {
				$output .= ', ';
			}
		}

		$output .= static::isAssoc($arr) ? "}" : "]";

		return $output;
	}
	private static function isAssoc(array $arr) {
		if (array() === $arr) return false;
		return array_keys($arr) !== range(0, count($arr) - 1);
	}
}