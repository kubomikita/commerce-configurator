<?php

class Sms {

	public static function send($to,$text){
		$fn=Config::smsModule.'_send';
		if(method_exists('Sms',$fn)){
			return Sms::$fn($to,$text);
		} else { return false; };
	}

	private static function smartsms_sk_send($to,$text){
		if(Config::sendSms){
			$to=trim(strtr($to,array(' '=>'','+'=>'')));
			if($to==''){ return false; };
			$c = curl_init();
			curl_setopt($c, CURLOPT_URL, 'http://www.smartsms.sk/api/send.do');
			curl_setopt($c, CURLOPT_POST, 1);
			curl_setopt($c, CURLOPT_SSL_VERIFYPEER,false);
			curl_setopt($c, CURLOPT_POSTFIELDS, 'username='.Config::smsUser.'&password='.md5(Config::smsPass).'&from='.Config::smsSender.'&to='.$to.'&message='.urlencode($text));
			curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($c, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)");
			curl_getinfo($c);
			$result = curl_exec($c);
			curl_close($c);
			return $result;

		};
	}






}

