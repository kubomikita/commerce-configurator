<?php

class Rounding {

	private $price;
	private $rounding = 0;

	public function __construct(float $price)
	{
		$this->price = round($price,2);
	}

	public function math2one(){
		$price = (float) sprintf("%.2f", $this->price);
		$this->rounding = round(($price - $this->price),2);
		//dump($this->price,$price);
		return $price;
	}

	public function math2tenth() {
		$price = (float) sprintf("%.1f0", $this->price);
		$this->rounding = round(($price - $this->price),2);
		//dump($this->price,$price);
		return $price;
	}

	public function math5cent() {
		$cents = (int) \Nette\Utils\Strings::substring(number_format($this->price, 2), -1);

		/*$centsFull = (int) Strings::substring(number_format($amount, 2), -2);
		$amountFull = round($amount + ($centsFull / 100),2);
		bdump($cents);*/
		$plus  = $minus = 0;
		if($cents < 3){
			$plus = 0;
		} elseif ($cents > 2 && $cents < 8) {
			$plus  = 0.05;
		} elseif ($cents >= 8 /*|| $cents == 0*/) {
			$plus  = 0.1;
		}
		//dump($plus,$minus,$cents,$this->price);
		$minus = $cents / 100;
		/*bdump($plus);
		bdump($minus);
		//bdump($amountFull);
		//bdump($centsFull);
		bdump($amount  + $minus);*/
		if($this->price > 0) {
			$price = (round($this->price - $minus + $plus, 2));
		} else {
			$price = (round($this->price + $minus - $plus, 2));
		}

		$this->rounding = round(($price - $this->price),2);

		return $price;
	}

	/**
	 * @return int
	 */
	public function getRounding()
	{
		return $this->rounding;
	}

}