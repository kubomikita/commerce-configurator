<?php


class ParamItem {
	public $type;
	public $text;
	public $key;

	function __construct($key,$text){
		$type='RAW';
		if(func_num_args()>=3){ $type=func_get_arg(2); };
		$this->type=$type;
		$this->text=LangStr($text);
		$this->key=$key;
	}

	public function input(){
		$ret='';
		if($this->type=='RAW'){
			$ret='<input type="text" class="textinput" name="param['.$this->key.']" value="'.Params::val($this->key).'" style="width: 450px;">';
		};
		if($this->type=='BOOL'){
			$ret='<input type="hidden" name="clearparam['.$this->key.']" value="1"><input type="checkbox"  name="param['.$this->key.']" value="1" '.(Params::val($this->key)?'checked':'').'>';
		};
		if($this->type=='CKE'){
			$ret='<textarea class="textinput cke" name="param['.$this->key.']" style="width: 450px;">'.Params::val($this->key).'</textarea>';
		};
		if($this->type == "textarea"){
			$ret='<textarea class="textinput" name="param['.$this->key.']" style="width: 450px;height:450px">'.Params::val($this->key).'</textarea>';
		}

		return $ret;
	}

}
