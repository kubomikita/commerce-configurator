<?php
use Nette\Caching\IStorage;
use Nette\Database\Connection;
use Nette\DI\Container;
use Nette\Utils\FileSystem;
use Nette\IOException;

class FileCache implements ICache{
	private static $cascade;

	public static $countHits = 0;
	public static $countMiss = 0;
	public static $listMiss = array();
	public static $listMissAll = array();
	public static $listHits = array();

	private static $DATA = array();

	public function setStorage( IStorage $storage ) { return; }

	static public function dir(){
		$dir = TEMP_DIR."/cache/";
		$context = \Kubomikita\Service::get("container");
		if($context->isHost("protein.cz")){
			$lg = "cz";
			if(!file_exists($dir.$lg)){
				mkdir($dir.$lg);
			}
			$dir = $dir.$lg."/";
		}
		return $dir;
	}

	static public function put($grp,$key,$data){
		try {
			if ( ! file_exists( FileCache::dir() . $grp ) ) {
				FileSystem::createDir( FileCache::dir() . $grp );
			};
			FileSystem::write( FileCache::dir() . $grp . "/" . $key, serialize( $data ) );
			//file_put_contents(FileCache::dir().$grp.'/'.$key,serialize($data),LOCK_EX);
		} catch (IOException $e){
			trigger_error($e->getMessage());
		}
	}

	static public function get($grp,$key){
		$data=FileCache::get_raw($grp,$key);
		$ret=unserialize($data);
		return $ret;
	}

	static public function get_raw($grp,$key){
		if(strlen(FileCache::$DATA[$grp.'/'.$key])){ $data=FileCache::$DATA[$grp.'/'.$key]; unset(FileCache::$DATA[$grp.'/'.$key]); }
		else { $data=file_get_contents(FileCache::dir().$grp.'/'.$key); };
		return $data;
	}

	public static function load(string $group, string $key, callable $callback) {
		if(self::check($group, $key)){
			return self::get($group, $key);
		}
		/** @var Container $container */
		$container = Registry::get("container")->container;
		$connection = $container->getByType( Connection::class);
		$result = $callback($connection, $container);
		self::put($group, $key, $result);
		return $result;
	}
	static public function check($grp,$key){
		//$ret=file_exists(FileCache::dir().$grp.'/'.$key);
		$ret=(FileCache::$DATA[$grp.'/'.$key]=@file_get_contents(FileCache::dir().$grp.'/'.$key));
		if($ret){
			if(!isset(Cache::$countHits)) Cache::$countHits=0;
			Cache::$countHits++;
			if(!isset(Cache::$listHits[$grp])) Cache::$listHits[$grp]=0;
			Cache::$listHits[$grp]++;
			$uns = @unserialize($ret);
			if($uns === false){
				//dump($uns." ".$grp."/".$key);
				//dblog('cache_fail_group',"$grp/$key");
				//dblog('cache_fail_string',$ret);
				//dblog('cache_fail_cookie',$_COOKIE);

				self::del($grp,$key);
				$ret = false;
			}

		} else {
			if(!isset(Cache::$countMiss)) Cache::$countMiss=0;
			Cache::$countMiss++;
			if(!isset(Cache::$listMiss[$grp])) Cache::$listMiss[$grp]=0;
			Cache::$listMiss[$grp]=$grp;
			//if(!isset(Cache::$listMissAll[$grp][$key])) Cache::$listMiss[$grp]=0;
			Cache::$listMissAll[$grp][$key]="$grp/$key";
		};
		//if(!$ret){ debug("$grp/$key",'Cache miss','INFO'); };
		//dump($grp."/".$key." - ".$ret);
		return $ret;
	}

	static public function flush($grp,$mask='*'){
		try {
			$dirs = FileCache::dirsDelete();
			if ( $mask == '*' ) {
				$create = [];
				$delete = [];
				foreach ( $dirs as $dir ) {
					//dump(file_exists( $dir . $grp ), $dir.$grp);
					if ( file_exists( $dir . $grp ) ) {
						FileSystem::rename( $dir . $grp, $dir . $grp . ".del" );
						$delete[] = $dir.$grp.".del";
						$create[] = $dir.$grp;
					}
				}

				foreach ($create as $c) {
					FileSystem::createDir($c);
				}
				foreach($delete as $d) {
					FileSystem::delete( $d );
				}
				//return $delete;
			} else {
				foreach ( $dirs as $dir ) {
					$files = glob( $dir . $grp . '/' . $mask );
					if ( is_array( $files ) && sizeof( $files ) ) {
						foreach ( $files as $file ) {
							if ( file_exists( $file ) ) {
								FileSystem::delete( $file );
							};
						};
					};
					/*if ( isset( FileCache::$cascade[ $grp ] ) && is_array( FileCache::$cascade[ $grp ] ) ) {
						foreach ( FileCache::$cascade[ $grp ] as $casc ) {
							Cache::flush( $casc );
						};
					};
					if ( $mask == "*" ) {
						if ( file_exists( $dir . $grp ) ) {
							\Nette\Utils\FileSystem::delete( $dir . $grp );
						}
					}*/
				}
			}
		} catch (IOException $e){
			trigger_error($e->getMessage());
			//return ["errorMessage" => $e->getMessage()];
		}

	}

	static public function del($grp,$mask='*'){
		static::flush($grp,$mask);
		/*$dirs = FileCache::dirsDelete();
		foreach($dirs as $dir) {
			$files = glob( $dir . $grp . '/' . $mask );
			if(is_array($files) and !empty($files)){
				foreach ($files as $file){
					unlink($file);
				}
			}
		}*/
	}

	static public function dirsDelete(){
		$ret = [];
		$langs = ["cz"];
		$maindir = FileCache::dir();
		$ret[] = $basedir = str_replace("/cz","",$maindir);
		foreach($langs as $lg){
			$ret[] = $basedir."".$lg."/";
		}

		//dump($ret);
		return $ret;
	}


	static public function cascade($src,$tgt){
		FileCache::$cascade[$src][]=$tgt;
	}

	static public function log($grp,$key,$data){
		if(!file_exists(FileCache::dir().$grp)){ mkdir(FileCache::dir().$grp,0777,1); };
		if(!file_exists(FileCache::dir().$grp.'/'.$key)){ mkdir(FileCache::dir().$grp.'/'.$key,0777,1); };
		$fx=fopen(FileCache::dir().$grp.'/'.$key.'/'.time().'_'.uniqid(),'w');
		fputs($fx,$data);
		fclose($fx);
	}

}