<?php
use \Nette\Caching\IStorage;

class NoCache implements ICache{
	
	public static function dir() {return;}

	/**
	 * @param IStorage $storage
	 */
	public function setStorage( IStorage $storage ) {}

	static public function put($grp,$key,$data){ return false; }
	static public function get($grp,$key){ return false; }
	static public function check($grp,$key){ return false; }
	public static function flush( $grp, $mask = '*' ) {return;}
	public static function del( $grp, $mask = '*' ) {return;}
	static public function cascade($src,$tgt){ return false; }
	public static function load( string $group, string $key, callable $callback ) {
		/** @var \Nette\DI\Container $container */
		$container = Registry::get("container")->container;
		$connection = $container->getByType(\Nette\Database\Connection::class);
		return $callback($connection, $container);
	}
}