<?php
use Nette\Caching\IStorage;

interface ICache {

	/**
	 * @param IStorage $storage
	 */
	public function setStorage( IStorage $storage );

	public static function dir();
	public static function check($grp,$key);
	public static function put($grp,$key,$data);
	public static function get($grp,$key);
	public static function del($grp, $mask = '*');
	public static function flush($grp, $mask = '*');
	public static function load(string $group, string $key, callable $callback);

	/**
	 * @deprecated
	 * @param $src
	 * @param $tgt
	 *
	 * @return mixed
	 */
	public static function cascade($src,$tgt);
}