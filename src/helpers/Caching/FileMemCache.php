<?php
use Nette\Caching\IStorage;

class FileMemCache extends FileCache implements ICache{
	
	private static $MEMCACHE = array();
	const MemSizeLimit = 32768;

	public static $countMemHits = 0;
	public static $listMemHits = array();

	public static $memSize = 0;
	public static $memOver = array();

	static public function flush($grp,$mask='*'){
		FileMemCache::$MEMCACHE = array();
		
		FileCache::flush($grp,$mask);
	}

	static public function del($grp,$mask='*'){
		FileMemCache::$MEMCACHE = array();
		FileCache::del($grp,$mask);
	}

	static public function check($grp,$key){
		//dump($grp,$key);
		if(isset(FileMemCache::$MEMCACHE[$grp.'/'.$key])){ return true; }
		else { return FileCache::check($grp,$key); };
	}

	static public function get($grp,$key){
		if(isset(FileMemCache::$MEMCACHE[$grp.'/'.$key])){
			if(!isset(Cache::$countMemHits)) Cache::$countMemHits=0;
			Cache::$countMemHits++;
			if(!isset(Cache::$listMemHits[$grp])) Cache::$listMemHits[$grp]=0;
			Cache::$listMemHits[$grp]++;

			return FileMemCache::$MEMCACHE[$grp.'/'.$key];
		} else {
			//$data=file_get_contents(FileCache::dir().$grp.'/'.$key);
			$data=FileCache::get_raw($grp,$key);
			$ret=unserialize($data);
			if(strlen($data)<=FileMemCache::MemSizeLimit){
				FileMemCache::$MEMCACHE[$grp.'/'.$key]=$ret;
				FileMemCache::$memSize+=strlen($data);
			} else { FileMemCache::$memOver[]=strlen($data); };
			return $ret;
		};
	}
	/*static public function update($grp,$key,$data){
		if(self::check($grp,$key)){
			file_put_contents(FileCache::dir().$grp.'/'.$key,serialize($data),LOCK_EX);
		} else {
			self::put($grp,$key,$data)
		}

	}*/

}