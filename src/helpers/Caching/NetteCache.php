<?php

//use Nette;

use \Nette\Caching\IStorage;
use \Nette\Caching\Storages\DevNullStorage;
use Nette\Caching\Cache;
use Nette\Database\Connection;
use Nette\DI\Container;

class NetteCache implements ICache {
	/**
	 * @var IStorage
	 */
	public static $storage;

	/**
	 * @param IStorage $storage
	 */
	public function setStorage( IStorage $storage ) {
		static::$storage = $storage;
	}

	/**
	 * @return IStorage
	 */
	public static function getStorage() : IStorage {
		if(static::$storage instanceof IStorage){
			return static::$storage;
		}
		return new DevNullStorage();
	}

	public static function dir() {
		return false;
	}

	public static function load(string $group, string $key, callable $callback) {
		$cache = new Cache(static::getStorage(), $group);
		return $cache->load($key, $callback);
	}

	public static function check( $grp, $key ) {
		$cache = new Cache(static::getStorage(), $grp);
		$data = $cache->load($key);
		return ($data !== null) ? true : false;
	}
	public static function put( $grp, $key, $data ) {
		$cache = new Cache(static::getStorage(), $grp);
		$cache->save($key, $data);
	}
	public static function get( $grp, $key ) {
		$cache = new Cache(static::getStorage(), $grp);
		return $cache->load($key);
	}
	public static function flush( $grp, $mask = '*' ) {
		try {
			/** @var Container $s */
			$s = \Kubomikita\Service::get( "container" )->getService( "container" );
			foreach ( $s->findByType( IStorage::class ) as $type ) {
				$cache = new Cache( $s->getService( $type ), $grp );
				$cache->clean( [/*Cache::ALL => true*/
					Cache::NAMESPACES => [ $grp ]
				] );

			}
		} catch (Exception $e){
			\Tracy\Debugger::log($e->getMessage());
		}
		//$cache = new Cache(static::getStorage(), $grp);
		//$cache->clean([Cache::ALL => true]);
	}
	public static function del( $grp, $mask = '*' ) {
		static::flush($grp,$mask);
	}
	public static function cascade( $src, $tgt ) {
		return;
	}
}