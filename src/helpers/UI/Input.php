<?php

class Input {
	public $type;
	public $name;
	public $value;
	public $cssClass = 'textinput';
	public $cssStyle;
	public $cssWidth = 200;
	public $items;
	public $selected;
	public $domId;
	public $showDefault=0;
	public $defaultName='';
	public $attrs=array();
	/**
	 * @param string $type[optional] <p>element type (text,hidden,password,textarea,select,selectg,radio,file)</p>
	 * @param string $name[optional] <p>field name</p>
	 * @param string $value[optional] <p>field value</p>
	 * @param array $items[optional] <p>items ($k=>$v) for select,selectg,radio</p>
	 *
	 */
	function __construct(){
		if(func_num_args()>=2){
			$this->type=func_get_arg(0);
			$this->name=func_get_arg(1);
		};
		if(func_num_args()>=3){
			$this->value=func_get_arg(2);
		};
		if(($this->type=='select' || $this->type=='selectg' || $this->type=='radio') && func_num_args()>=4){
			$this->items=func_get_arg(3);
		};
		if(($this->type=='radiobox') && func_num_args()>=4){
			$this->selected=func_get_arg(3);
		};
		if(($this->type=='select' || $this->type=='selectg') && func_num_args()>=5){
			$this->showDefault=1;
			$this->defaultName=func_get_arg(4);
		};
	}

	public function width($w){ $this->cssWidth=(int)$w; return $this; }
	public function cssStyle($x){ $this->cssStyle=$x; return $this; }
	public function cssClass($x){ $this->cssClass=$x; return $this; }
	public function bold(){ $this->cssStyle.='font-weight: bold;'; return $this; }
	public function domId($x){ $this->domId=$x; return $this; }
	public function setAttr($attr,$value) {
		$this->attrs[$attr]=$value;
		return $this;
	}

	function __toString(){
		$ret='';
		if($this->type=='text'){
			$ret='<input type="text" ';
			foreach($this->attrs as $name =>$value){
				$ret.= $name.'="'.$value.'" ';
			}

			$ret.=' id="'.$this->domId.'" class="'.$this->cssClass.'" style="width: '.$this->cssWidth.'px; '.$this->cssStyle.'" name="'.$this->name.'" value="'.$this->value.'">';
		};
		if($this->type=='langtext'){
			$elid=Content::getId();
			$ret='<div class="langstr_input" style="width: '.$this->cssWidth.'px;" id="'.$elid.'">';
			$ret.='<div class="inputs" id="'.$this->domId.'">';
			if(!($this->value instanceof LangStr)){ $this->value=new LangStr($this->value); };
			foreach(Config::$publicLanguages as $lang){
				$ret.='<input ';
				if($lang == "sk"){
					foreach($this->attrs as $name =>$value){
						$ret.= $name.'="'.$value.'" ';
					}
				}
				$ret.=' ref="'.$lang.'" type="text" class="'.$this->cssClass.'" style="'.($lang==LangStr::getLang()?'':'display: none;').' width: '.($this->cssWidth-4).'px; '.$this->cssStyle.'" name="'.$this->name.'['.$lang.']" value="'.$this->value->lang($lang).'">';
			};
			$ret.='</div>';

			$ret.='<div class="languages">';
			foreach(Config::$publicLanguages as $lang){
				$ret.='<div class="ilng_btn '.($lang==LangStr::getLang()?'ilng_btn_active':'').'" val="'.$lang.'"><img src="img/16/flag_'.$lang.'.png"></div>';
			};
			$ret.='<div class="cleaner"></div></div>';

			$ret.='</div>';
			//$ret='<input type="text" class="'.$this->cssClass.'" style="width: '.$this->cssWidth.'px; '.$this->cssStyle.'" name="'.$this->name.'" value="'.$this->value.'">';
		};
		if($this->type=='hidden'){
			$ret='<input type="hidden" name="'.$this->name.'" value="'.$this->value.'">';
		};
		if($this->type=='password'){
			$ret='<input type="password" id="'.$this->domId.'" class="'.$this->cssClass.'" style="width: '.$this->cssWidth.'px; '.$this->cssStyle.'" name="'.$this->name.'" value="'.$this->value.'">';
		};
		if($this->type=='textarea'){
			$ret='<textarea id="'.$this->domId.'" ';
			foreach($this->attrs as $name =>$value){
				$ret.= $name.'="'.$value.'" ';
			}

			$ret.=' class="'.$this->cssClass.'" style="width: '.$this->cssWidth.'px; max-width: '.$this->cssWidth.'px; '.$this->cssStyle.'" name="'.$this->name.'">'.$this->value.'</textarea>';
		};
		if($this->type=='langtextarea'){
			$elid=Content::getId();
			if(!($this->value instanceof LangStr)){ $this->value=new LangStr($this->value); };
			$ret='<div class="langtxt_input" style="width: '.$this->cssWidth.'px;" id="'.$elid.'">';
			$ret.='<div class="inputs" id="'.$this->domId.'">';
			foreach(Config::$publicLanguages as $lang){
				$ret.='<div ref="'.$lang.'" style="'.($lang==LangStr::getLang()?'':'display: none;').'" class="textarea">
                  <textarea ';
				if($lang == "sk"){
					foreach($this->attrs as $name =>$value){
						$ret.= $name.'="'.$value.'" ';
					}
				}
				$ret.=' class="'.$this->cssClass.'" style="width: '.($this->cssWidth-4).'px; max-width: '.($this->cssWidth-4).'px; '.$this->cssStyle.'" name="'.$this->name.'['.$lang.']" >'.$this->value->lang($lang).'</textarea></div>';
			};
			$ret.='</div>';

			$ret.='<div class="languages">';
			foreach(Config::$publicLanguages as $lang){
				$ret.='<div class="ilng_btn '.($lang==LangStr::getLang()?'ilng_btn_active':'').'" val="'.$lang.'"><img src="img/16/flag_'.$lang.'.png"></div>';
			};
			$ret.='<div class="cleaner"></div></div>';

			$ret.='</div>';
			//$ret='<input type="text" class="'.$this->cssClass.'" style="width: '.$this->cssWidth.'px; '.$this->cssStyle.'" name="'.$this->name.'" value="'.$this->value.'">';
		};
		if($this->type=='select'){
			$ret='<select id="'.$this->domId.'" class="'.$this->cssClass.'" style="width: '.$this->cssWidth.'px; '.$this->cssStyle.'" name="'.$this->name.'">';
			if($this->showDefault){ $ret.='<option value="">'.$this->defaultName.'</option>'; };
			if(sizeof($this->items)){ foreach($this->items as $k=>$t){
				$style="";
				if(strstr($t,'<b>')){
					$t=str_replace('<b>','',$t);
					$style="color: red;";
				};
				if(strstr($t,'<i>')){
					$t=str_replace('<i>','',$t);
					$style="color: #00df00;";
				};
				$ret.='<option style="'.$style.'" value="'.$k.'" '.(($k==$this->value)?'selected':'').'>'.$t.'</option>';
			};};
			$ret.='</select>';
		};
		if($this->type=='selectg'){
			$ret='<select id="'.$this->domId.'" class="'.$this->cssClass.'" style="width: '.$this->cssWidth.'px; '.$this->cssStyle.'" name="'.$this->name.'">';
			if($this->showDefault){ $ret.='<option value="">'.$this->defaultName.'</option>'; };
			if(sizeof($this->items)){ foreach($this->items as $grp=>$itm){
				$ret.='<optgroup label="'.$grp.'">';
				if(sizeof($itm)){ foreach($itm as $k=>$t){
					$ret.='<option value="'.$k.'" '.(($k==$this->value)?'selected':'').'>'.$t.'</option>';
				};};
				$ret.='</optgroup>';
			};};
			$ret.='</select>';
		};
		if($this->type=='radio'){
			if(sizeof($this->items)){ foreach($this->items as $k=>$t){
				$ret.='<label style="display:block;"><input type="radio" name="'.$this->name.'" value="'.$k.'" '.(($k==$this->value)?'checked':'').'> '.$t.'</label>';
			};};
		};
		if($this->type=='file'){
			$ret='<input type="file" class="'.$this->cssClass.'" style="'.$this->cssStyle.'" name="'.$this->name.'">';
		};
		if($this->type=='checkbox'){
			$ret='<input id="'.$this->domId.'" type="checkbox" name="'.$this->name.'" value="1" '.(($this->value)?'checked':'').'>';
		};
		if($this->type=='radiobox'){
			$ret='<input id="'.$this->domId.'" type="radio" name="'.$this->name.'" value="'.$this->value.'" '.(($this->selected)?'checked':'').'>';
		};
		return $ret;
	}

}