<?php

class UI {

	public static function Iframe($url){ return new Iframe($url); }

	public static function Input(){
		$args=func_get_args();
		$ret=new Input();
		call_user_func_array(array($ret,'__construct'),$args);
		return $ret;
	}

	public static function LargeButton($icon,$text,$action){
		$width=150;
		if(func_num_args()>=4){ $width=func_get_arg(3); };
		return '<div class="button noprint" onclick="'.$action.'" style="width: '.$width.'px;"><div><img src="'.$icon.'">'.$text.'</div></div>';
	}

	public static function ButtonsBar($content){
		return '<div class="buttonbar noprint">'.$content.'<div class="cleaner"></div></div>';
	}

	public static function Help($content){
		return '<div class="helpbutton"><div class="helpbox">'.(string)$content.'</div></div>';
	}
}