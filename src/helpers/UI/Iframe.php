<?php
class Iframe {
	public $url;
	public $cssClass = '';
	public $cssStyle;
	public $cssWidth = '100%';
	public $cssHeight = '200px;';
	public $domId = '';

	function __construct($url){
		$this->url=$url;
	}

	public function cssClass($x){ $this->cssClass=$x; return $this; }
	public function cssStyle($x){ $this->cssStyle=$x; return $this; }
	public function cssWidth($x){ $this->cssWidth=$x; return $this; }
	public function cssHeight($x){ $this->cssHeight=$x; return $this; }
	public function domId($x){ $this->domId=$x; return $this; }

	function __toString(){
		return '<iframe src="'.$this->url.'" id="'.$this->domId.'" class="'.$this->cssClass.'" style="height: '.$this->cssHeight.'; width: '.$this->cssWidth.'; '.$this->cssStyle.'"></iframe>';
	}
}