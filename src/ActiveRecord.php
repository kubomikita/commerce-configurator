<?php

use Nette\Database\Connection;
use Nette\Database\Context;
use Nette\Database\Table\ActiveRow;
use Nette\Database\Table\Selection;
use Nette\Reflection\ClassType,
	Nette\Reflection\Property;
use Nette\Utils\Strings;
use Kubomikita\Service;

/**
 * @property-read Connection $db
 * @property-read Context $dbContext
 * @property-read Selection $model
 * @property-read ActiveRow $activeRow
 * @property-read Container $context
 * @property-read Container $container
 * @property-read Request $request
 * @property-read Request $httpRequest
 * @property-read Configurator $configurator
 */
abstract class ActiveRecord {
	use Nette\SmartObject;

	const MAIN_TABLE = 1;
	const DATA_TABLE = 0;

	protected static $instances = [];
	protected static $inject = [];
	protected static $table = null;
	protected static $table_images = null;
	protected static $select = ["t.*"];
	/** @var null|Nette\Database\Connection */
	/*protected $db = null;*/
	/** @var null|Nette\Reflection\ClassType */
	//protected $classType = null;
	protected $create = false;
	protected $initialized = [];

	protected $ROW;
	//protected $import = [];

	public function row() : ActiveRow|\Nette\Database\Row
	{
		return $this->ROW;
	}

	public static function flushInstances(){
		static::$instances = [];
	}

	public static function getInstances(){
		return static::$instances;
	}

	private function dataInit($data){
		if(is_numeric($data)) {
			if ( $data == "0" or $data == 0 ) {
				$data = null;
			}
		}
		if(is_string($data)){
			if(trim(strlen($data)) == 0 or (int) $data == 0){
				$data = null;
			}
		}
		return $data;
	}
	public static function fetchAll($where = []){
		$order = null;
		if(func_num_args() > 1){
			$order = func_get_arg(1);
		}

		$cache_name = "fetch_".md5(serialize($where));

		return Cache::load(static::getCacheGroup(), $cache_name, function (&$dependencies /*Connection $db*/) use($where, $order) {
			//$dependencies = [/*\Nette\Caching\Cache::TAGS => ["ahoj", "čau"],*/ \Nette\Caching\Cache::EXPIRE => '+1 minute'];
			//bdump($where,"fetch_all ". static::$table);
			//bdump($dependencies);
			$db = Registry::get("database");
			$ret = [];
			if($order !== null){
				$q = $db->query( "SELECT * FROM " . static::$table . " WHERE", $where," ORDER BY ".$order );
			} else {
				$q = $db->query( "SELECT * FROM " . static::$table . " WHERE", $where );
			}
			foreach ( $q as $r ) {
				$ret[] = new static( $r );
			}
			return $ret;
		});

		return $ret;
	}

	public static function findAll() : Selection{
		/** @var Context $db */
		$db = Registry::get("database.context");
		return $db->table(static::$table);
	}

	public function toArray($exclude = []): array
	{
		$r = new ReflectionClass($this);
		$array = array();
		foreach($r->getProperties(ReflectionProperty::IS_PUBLIC) as $property){
			if(!in_array($property->getName(), $exclude)) {
				$array[$property->getName()] = $this->{$property->getName()};
			}
		}

		return $array;
	}

	public function __construct($data=null) {
		$data = $this->dataInit($data);
		if(static::$table === null){
			throw new Exception("Table name must be filled. Please specify 'protected static \$table'",500);
		}
		$db = $this->initDatabase();
		$R = [];
		if($data !== null){
			//dump(is_numeric($data));exit;
			if(is_numeric($data)){
				$id = (int) $data;
				if(isset(static::$instances[static::getCacheGroup()][$id])){
					$R = static::$instances[static::getCacheGroup()][$id];
				} else {
					$R = Cache::load(static::getCacheGroup(), $id, function (&$dependencies) use($db, $id){
						$query = "select " . implode( ",",static::$select ) . " from " . static::$table. " as t where t.id=?";
						$q     = $db->query( $query, $id );
						return $q->fetch();
					});
					static::$instances[static::getCacheGroup()][$id] = $R;
				}
			} elseif($data instanceof Nette\Database\Row) {
				$R = $data;
			} elseif ($data instanceof ActiveRow){
				$R = $data->toArray();
			} else {
				throw new Exception(get_called_class() ." - Unsuported type of data.");
			}
			if($R instanceof Nette\Database\Row || $data instanceof ActiveRow){
				$this->ROW = $R;
				$this->initProperties($R);
			}
		} else {
			$properties = $this->getProperties();
			foreach($properties as $property){
				$propertyName = $property->getName();
				$annotation =   $property->getAnnotation("var");
				if(is_string($annotation) && class_exists($annotation)){
					$this->{$propertyName} = new $annotation();
				}
			}
		}
	}

	/**
	 * @param \Nette\Database\Row $R
	 * @param int $main
	 */
	protected function initProperties(/*\Nette\Database\Row*/ $R,$main = self::MAIN_TABLE){
		$properties = $this->getProperties();
		foreach($properties as $property){
			$annotations = $property->getAnnotations();
			$annotations_count = count($annotations);
			$dataTable = $property->getAnnotation("data");
			if ( ! in_array( $property->getName(), $this->initialized ) ) {
				if ($main == self::MAIN_TABLE ) {
					if(!$dataTable) {
						$this->initAnnotations( $R, $property, $annotations_count );
						$this->initialized[] = $property->getName();
					}
				} else {
					$annotations_count --;
					$this->initAnnotations($R,$property,$annotations_count);
					$this->initialized[] = $property->getName();
				}
			}
		}
	}

	/**
	 * @param \Nette\Database\Row $R
	 * @param $property
	 * @param $annotations_count
	 */
	private function initAnnotations(/*\Nette\Database\Row*/ $R,$property,$annotations_count){
		$properties = $this->getProperties();
		$propertyName = $property->getName();
		if ( $annotations_count < 2 ) {
			$this->{$propertyName} = static::prepareDataOnLoad( $R[ $propertyName ],
				$property->getAnnotation( "var" ), $propertyName );
			//dump($this->{$propertyName},$propertyName);
		} else {
			$source = $property->getAnnotation( "source" );
			if ( $source ) {
				if ( isset( $properties[ $source ] ) ) {
					$meta = static::prepareDataOnLoad( $R[ $source ],
						$properties[ $source ]->getAnnotation( "var" ), $propertyName );
					$this->{$propertyName} = $meta[ $property->getAnnotation( "var" ) ];
				} else {
					$this->{$propertyName} = static::prepareDataOnLoad( $R[ $source ],
						$property->getAnnotation( "var" ), $propertyName );
				}
			}
		}
	}

	/**
	 * @param $data
	 *
	 * @return mixed
	 * @throws Exception
	 */
	protected function getRow($data){
		if(is_numeric($data)){
			return static::$instances[static::getCacheGroup()][(int)$data];
		} elseif($data instanceof Nette\Database\Row){
			return $data;
		}
		throw new Exception("Data row is empty");
	}

	/**
	 * @return array
	 */
	private function getProperties(){
		$ref =/* $this->classType =*/ new ClassType($this);
		$properties = [];
		foreach($ref->getProperties(Property::IS_PUBLIC ) as $val){
			if($val->isStatic()){
				continue;
			}
			$properties[$val->getName()] = $val;
		}
		return $properties;
	}

	/**
	 * @param $data
	 * @param null $var
	 * @param null $propertyName
	 *
	 * @return array|LangStr|mixed|Timestamp
	 */
	protected static function prepareDataOnLoad($data,$var=null,$propertyName = null){
		if($var == "json"){
			//dump($data);
			//($data===null)?$data=[]:$data;
			return json_decode($data, flags: JSON_OBJECT_AS_ARRAY);
		} elseif ($var == 'datetime') {
			return $data !== null ? \Nette\Utils\DateTime::from($data) : null;
		} elseif($var == "LangStr") {
			return new LangStr($data);
		} elseif($var == "Timestamp"){
			return new Timestamp($data);
		} elseif($var == "serialized"){
			return unserialize($data ?? "");
		} elseif(is_string($var) && strpos($var,"import") !== false){
			list($variable,$class) = explode("|",$var);
			//$this->import[$propertyName] = ["class"=>$class,"data"=>$data];

			$tagy = self::importTag($class,$data);

			//bdump($tagy);
			return $tagy;
		}
		return $data;
	}

	protected static function importTag($class,$data){
		$ids = [];
		foreach((array)explode(']',$data ?? '') as $tag){
			$tag=trim(strtr($tag,array('['=>'',']'=>'')));
			if($tag!=''){
				$ids[] = (int) $tag;
			};
		}
		return $class::getByIds($ids);
	}

	public static function getSelect(){
		return static::$select;
	}
	public static function getCacheGroup(){
		return static::$table;
	}
	protected function initDatabase() : Connection{
		return Registry::get( "database" );
	}
	public function __get( $name ) {
		if ( $name == "db" ) {
			return Service::getByType( Connection::class );
		} elseif ($name == "dbContext") {
			return Service::getByType( Context::class );
		} elseif ($name == "model"){
			return $this->dbContext->table( static::$table );
		} elseif ($name == "activeRow"){
			return $this->model->get($this->id);
		} elseif ($name == "container" || $name == "context") {
			return Service::getByType(\Nette\DI\Container::class);
		} elseif($name == "request" || $name == "httpRequest"){
			return Service::getByType(\Nette\Http\Request::class);
		} elseif($name == "configurator") {
			return Service::getByType(\Kubomikita\Commerce\IDiConfigurator::class);
		}

		$class = get_class($this);
		$properties = $this->getProperties();
		$property = $name."_id";
		if(isset($properties[$property])) {
			/** @var Property $p */
			$p = $properties[$property];
			$value = $this->{$p->getName()};
			$inject = $p->getAnnotation("inject");
			if($inject === null){
				throw new Exception("Inject annotation missing.",500);
			}
			$hash = md5($inject.$value.(isset($this->id)?$this->id:""));
			if($inject=="Partner") {
				//dump( $hash, $inject, $value, $this );
			}

			if(isset(static::$inject[static::getCacheGroup()][$hash])){
				return static::$inject[static::getCacheGroup()][$hash];
			} else {
				if($value !== null && trim($value) == ""){
					$value =null;
				}
				$obj = new $inject($value);
				/*if($inject == "Tovar" && (isset($properties["variant"]) || isset($properties["Variant"]))){
					$variant = $this->variant;
					if(!empty($variant)) {
						$obj->Variant = $variant;
						$obj->variant_load();
					}
				}*/
				$obj = $this->afterGet($inject,$value,$properties,$obj,$this);
				static::$inject[static::getCacheGroup()][$hash] = $obj;
				return $obj;
			}

		} else {
			throw new \Nette\MemberAccessException("Cannot read undeclared property $class::$name");
		}
	}
	protected function afterGet($inject,$value,$properties,$obj,$parent){

		//dump($inject,$value,$properties,$obj,$parent);
		if($inject == "Tovar" && (isset($properties["variant"]) || isset($properties["Variant"]))){
			$variant = $parent->variant;
			if(!empty($variant)) {
				$obj->Variant = $variant;
				$obj->variant_load();
			}
		}
		return $obj;
	}
	public function __set( $name, $value ) {
		$class = get_class($this);
		$properties = $this->getProperties();
		$property = $name."_id";
		//dump($name,$value);
		if(isset($properties[$property])) {
			/** @var Property $p */
			//$p = $properties[ $property ];
			$this->{$name} = $value;
			$this->{$property} = $value->id;
		} else {
			throw new \Nette\MemberAccessException("Cannot write undeclared property $class::$name");
		}
	}

	public static function getByIds(array $ids){
		$ret = [];
		$list =get_called_class()."listid".md5(serialize($ids));

		if(!empty($ids)) {

			if(isset(static::$instances[$list])){
				return static::$instances[$list];
			} else {
				$instance                   = Cache::load( static::getCacheGroup(), $list, function () use ( $ids ) {
					$ret = [];
					/** @var Connection $db */
					$db = Registry::get( "database" );
					$q  = $db->query( "SELECT " . implode( ",",
							static::$select ) . " FROM " . static::$table . " as t WHERE t.id IN (?) ORDER BY FIELD(t.id,?)",
						$ids, $ids );
					foreach ( $q as $r ) {
						$ret[ $r->id ] = new static( $r );
					}

					return $ret;
				} );
				static::$instances[ $list ] = $instance;
				return $instance;
			}

		}
		return $ret;
	}
	public function images(){
		if(static::$table_images === null){
			throw new Exception("Images table name is null");
		}

		return Cache::load(static::getCacheGroup(), $this->id."_images", function (){
			$ret = [];
			$reflection = new ReflectionClass($this);
			$shortName = $reflection->getShortName();
			$object = str_replace("Model","", $shortName);
			$objectImgModel = $object."ImgModel";//Model
			$query = "select * from ".static::$table_images." where `" . Strings::lower($object) . "`=? order by poradie asc";
			$q = $this->db->query($query,$this->id);

			foreach($q as $i){
				$name = (isset($this->nazov))?(string)$this->nazov:null;
				$ret[] = new $objectImgModel($i,$name);
			}
			return $ret;

		});

	}
	// prvy obrazok
	public function image(){
		$imgs=$this->images();
		if(!empty($imgs)){
			return $imgs[0];
		}
		return new BlankImg();
	}
	protected function beforeSave(){

	}
	protected function afterSave(){

	}
	private function initSaveAnnotations($propertyName,$property,$annotations_count,$properties){
		$dataset = $this->$propertyName;
		$data = null;
		if($property->getAnnotation("inject")){
			$dd = str_replace("_id","",$propertyName);
			$dataset = $this->$dd->id;
		}

		$global = $property->getAnnotation("global");
		if(!$global) {
			if ( $annotations_count < 2 ) {
				$data[ $propertyName ] = static::prepareDataOnSave( $dataset,
					$property->getAnnotation( "var" ) );
			} else {
				$source = $property->getAnnotation("source");
				if($source) {
					if(isset($properties[$source])) {
						//echo '<hr>';
						//dump($property->getAnnotation("var"),$propertyName,$dataset,$source);
						if($property->getAnnotation( "var" ) !== $propertyName){
							$propertyName = $property->getAnnotation( "var" );
						}
						//dump($source,$propertyName,$data,$dataset,static::prepareDataOnSave($this->$source,$properties[$source]->getAnnotation("var")));
						//if($dataset !== null) {
						$this->{$source}[ $propertyName ] = $dataset;
						//$this->fa_dan = array();
						//}
						$meta = static::prepareDataOnSave($this->$source,$properties[$source]->getAnnotation("var"));
						$data[$source] = $meta;

					} else {
						$data[$source] = static::prepareDataOnSave($this->{$propertyName},$property->getAnnotation("var"));
					}
				}
			}
		}
		return $data;
	}
	private function test(){
		foreach($properties as $property){
			$annotations = $property->getAnnotations();
			$annotations_count = count($annotations);
			$dataTable = $property->getAnnotation("data");
			if ( ! in_array( $property->getName(), $this->initialized ) ) {
				if ($main == self::MAIN_TABLE ) {
					if(!$dataTable) {
						$this->initAnnotations( $R, $property, $annotations_count );
						$this->initialized[] = $property->getName();
					}
				} else {
					$annotations_count --;
					$this->initAnnotations($R,$property,$annotations_count);
					$this->initialized[] = $property->getName();
				}
			}
		}
	}
	protected function initSaveParameters($main = self::MAIN_TABLE){
		$properties = $this->getProperties();
		$data = [];
		foreach($properties as $property){
			$propertyName = $property->getName();
			$annotations = $property->getAnnotations();
			$annotations_count = count($annotations);
			$dataTable = $property->getAnnotation("data");
			if($main == self::MAIN_TABLE){
				if(!$dataTable){
					$return = $this->initSaveAnnotations($propertyName,$property,$annotations_count,$properties);
					//dump( $return, "return " . $propertyName,$properties,$property );
					if($return !== null) {
						//bdump( $return, "return " . $propertyName );
						$key = key( $return );
						if ( $key !== null ) {
							$val          = $return[ $key ];
							$data[ $key ] = $val;
						}
						//	dump($data);
					}
				}
			} elseif($main == self::DATA_TABLE){
				if($dataTable) {
					$annotations_count --;
					$return = $this->initSaveAnnotations( $propertyName, $property, $annotations_count,$properties );
					$key = key($return);
					if($key !== null) {
						$val          = $return[ $key ];
						$data[ $key ] = $val;
					}
				}
			}


		}

		return $data;
	}

	private function setProperties($data){
		$properties = $this->getProperties();
		//dump($data);
		if(!empty($data)) {
			foreach ( $data as $k => $v ) {
				if ( ! isset( $properties[ $k ] ) ) {
					throw new \Nette\Application\AbortException( "Property with key '$k' in class '" . get_called_class() . "' not exists!" );
				} else {
					$this->{$k} = $v;
				}
			}
		}
	}

	public function save($data = []){
		if(!empty($data) and is_array($data)){
			$this->setProperties($data);
		}
		if(!$this->id){ $this->id=mysqli_new(static::$table); $this->create = true; };

		$this->beforeSave();
		$data = $this->initSaveParameters();
		//bdump($data);
		unset($data["id"]);

		try {
			$q = $this->db->query("UPDATE ".static::$table." SET ",$data,"WHERE id = ?",$this->id);
			//echo $q->getQueryString();
			//dump($data,get_called_class(),debug_backtrace());
			//bdump($this->id."*","cache delete".static::getCacheGroup());
			Cache::flush(static::getCacheGroup(),$this->id."*");
			Cache::flush(static::getCacheGroup(),"*list*");
		} catch(Exception $e){
			//echo $q->getQueryString();
			dump($data);
			echo $e->getMessage();exit;
			trigger_error($e->getMessage(),$e->getCode());
			\Tracy\Debugger::log($e->getMessage(),$e->getCode());
		}
		$this->afterSave();

		if($this->create){
			return $this->id;
		}
		//dump($data);

	}
	public static function prepareDataOnSave($data,$var){
		//dump($data,$annotation);
		if($var == "serialized") {
			return serialize($data);
		} elseif($var == "LangStr"){
			return $data->json();
		} elseif($var == "json"){
			return json_encode($data);
		} elseif($var == "Timestamp"){
			$val = $data->val();
			return $val;
		} elseif($var !== null && strpos($var,"import") !== false){
			$ret='';
			if(!empty($data)) {
				foreach ( $data as $id => $object ) {
					$ret .= '[' . $id . ']';
				}
			}

			return $ret;
		}
		return $data;
	}
	protected function deletable(){
		return true;
	}
	public function delete(){
		if($this->id && $this->deletable()){
			//echo "delete";
			$this->db->query("DELETE FROM ".static::$table." WHERE id=?",$this->id);
			//dump($this);
			Cache::flush(static::getCacheGroup());
			//$this->id = null;
		}
	}
}




class Cron extends ActiveRecord {
	protected static $table = "ec_cron";
	/** @var int */
	public $id;
	/** @var string */
	public $name;
	/** @var int */
	public $enabled;
	/** @var string */
	public $model;
	/** @var json */
	public $args;
	/** @var string */
	public $minute;
	/** @var datetime */
	public $last;
}

class Stat extends ActiveRecord {
	protected static $table = "ec_countries";
	public $id;
	public $pohoda_id;
	public $skratka;
	public $predvolba;
	/** @var LangStr */
	public $nazov;
	public $currency;
	public $metadata;
	public $eu;
	public $visible;
}

class Obj extends ActiveRecord {
	protected static $table = "ec_objednavky";
	public $id;
	public $cislo;
	/** @var Timestamp */
	public $datum;
	/** @var serialized */
	public $metadata;
	/**
	 * @var faktura
	 * @source metadata
	 */
	public $faktura;
	/**
	 * @var test
	 * @source metadata
	 */
	public $test;
}