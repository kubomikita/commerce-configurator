<?php
namespace Kubomikita\Commerce;

use http\Exception\InvalidArgumentException;
use Kubomikita\Utils\Arrays;
use Nette;
use Nette\DI;
use Nette\Http\Url;
use Nette\Utils\Validators;
use Tracy;

use Nette\DI\Container;
use Nette\InvalidStateException;
use Nette\Loaders\RobotLoader;


use Kubomikita\Service;
use Nette\Application\AbortException;
use Nette\Neon\Neon;
use Nette\Utils\FileSystem;
use Nette\Utils\Strings;

abstract class DiConfigurator implements IDiConfigurator {
	/** @var AppRouter */
	public $router = null;
	/** @var array Of controllers */
	public $controllers = [];

	const COOKIE_SECRET = "commerce_debug";
	/** @var callable[]  */
	public $onCompile;
	/** @var callable[] */
	public $onStart;
	/** @var array */
	protected $parameters;
	protected $parameters_raw;
	/** @var array  */
	protected $files = [];
	/** @var array */
	protected $routers = [
		"cli" => \Kubomikita\Cli\BaseRouter::class, //"\\Kubomikita\\Cli\\BaseRouter",
		"redirects" => null,
		"ajax" =>  \Kubomikita\Ajax\Router::class,
		"api" => \Kubomikita\Api\Router::class,
		//"application" => "\\Kubomikita\\Commerce\\Router",
	];
	/** @var Container */
	public $container;
	/** @var bool */
	protected $debuggerEnabled = false;
	/** @var string */
	protected $debuggerPanelService = "\\Kubomikita\\Commerce\\TracyPanelService";

	/**
	 * Set class name of CLI router
	 * @param $class string
	 */
	public function setCliRouter($class){
		$this->routers["cli"] = $class;
	}

	/**
	 * Set class name of API router
	 * @param $class string
	 */
	public function setApiRouter($class){
		$this->routers["api"] = $class;
	}

	/**
	 * Set class name of Application router
	 * @param $class string
	 */
	public function setAppRouter($class){
		$this->routers["application"] = $class;
	}
	public function setAjaxRouter($class){
		$this->routers["ajax"] = $class;
	}
	public function setRedirectsRouter($class){
		$this->routers["redirects"] = $class;
	}

	/**
	 * @deprecated
	 * @param $c
	 *
	 * @throws \Exception
	 */
	public function loadService($c){

	}

	/**
	 * @deprecated
	 * @param array $list
	 *
	 * @throws \Exception
	 */
	public function createRegister(array $list = []){

	}

	public function createContainer( $file = APP_DIR . "config/services.neon" ) {
		if(!file_exists($file)){
			throw new AbortException("Config with services not exists");
		}

		$containerDir = TEMP_DIR."cache/_Nette.Container.Ecommerce";
		if(!file_exists($containerDir)){
			FileSystem::createDir($containerDir);
		}
		$loader = new DI\ContainerLoader($containerDir,$this->isDebugMode());
		$class = $loader->load(function($compiler) use ($file){
			/** @var DI\Compiler $compiler */
			$compiler->loadConfig($file);
		});

		$this->parameters_raw = $this->parameters;
		//dump($this->parameters_raw);
		$this->parameters = $this->expandParams($this->parameters);
		/** @var Container container */
		$this->container = new $class;
		$this->container->addService("configurator",$this);
		$this->container->parameters = $this->parameters;

		Service::add("context",$this->container);
		if($this->container->hasService("session")){
			$this->onCompile[] = function(){
				$this->container->getService("session")->start();
			};
		}

		Tracy\Debugger::getBar()->addPanel(new Nette\Bridges\DITracy\ContainerPanel($this->container));

		if($this->onCompile !== null) {
			if (!is_array($this->onCompile) && !$this->onCompile instanceof \Traversable) {
				throw new Nette\UnexpectedValueException('Property Form::$onValidate must be array or Traversable, ' . gettype($this->onCompile) . ' given.');
			}
			foreach($this->onCompile as $handler){
				$params = Nette\Utils\Callback::toReflection($handler)->getParameters();
				$values = isset($params[1]) ? $this->getValues($params[1]->isArray()) : null;
				$handler($this, $values);
			}
		}
		if($this->container->hasService("form.factory")){
			$this->onStart[] = function() {
				//$this->container->getService("form.factory")->register();
				$this->container->getService("form.factory")->listen();
			};
		}
		$allowedHosts = $this->container->parameters["allowedHosts"] ?? [];
		$redirectHosts = $this->container->parameters["redirectHosts"] ?? [];

		$this->onStart[] = function () use($allowedHosts, $redirectHosts){
			if(!empty($redirects = $redirectHosts) && !$this->isCli()){
				$host = $this->container->getByType(Nette\Http\Request::class)->getUrl()->getHost();
				if(isset($redirects[$host])){
					$redirect = $redirectHosts[$host];
					bdump($redirects, "redirect hosts");
					if(Validators::isUrl($redirect)){
						$redirectUrl = new Url($redirect);
						if((string) $this->container->getByType(Nette\Http\Request::class)->getUrl()->withQuery([]) !== $redirect &&  $host !== $redirectUrl->getHost()) {
							$this->container->getByType(Nette\Http\Response::class)->setCode(302);
							$this->container->getByType(Nette\Http\Response::class)->redirect($redirectUrl);
							exit;
						} else {
							trigger_error("Redirect host must be other than main host.");
						}
					} else {
						trigger_error("Redirect '$redirect' is not valid URL.");
					}

				}
			}
			bdump($this, "app starting");
			bdump($_SERVER, "server info");
			if(!empty($hosts = $allowedHosts) && !$this->isCli()) {
				if(!in_array(($host = $this->container->getByType(Nette\Http\Request::class)->getUrl()->getHost()), $hosts)){
					bdump($allowedHosts, "Allowed hosts");
					$this->container->getByType(Nette\Http\Response::class)->setCode(403);
					die($host.' is not allowed.');
				}
			}
		};
		return $this->container;
	}

	/**
	 * @param Page $Page
	 * @param AppRouter $Router
	 *
	 * @return string
	 */
	/*public function run( $Page, $Router ) {
		dump($this);
	}*/
	public function getRouters(){
		return $this->routers;
	}
	/**
	 * Creates routers
	 * @param array $list
	 *
	 * @throws Nette\Application\BadRequestException
	 */
	public function createRouter(array $list){

		if($this->onStart !== null) {
			if (!is_array($this->onStart) && !$this->onStart instanceof \Traversable) {
				throw new Nette\UnexpectedValueException('Property Form::$onValidate must be array or Traversable, ' . gettype($this->onStart) . ' given.');
			}
			foreach($this->onStart as $handler){
				$params = Nette\Utils\Callback::toReflection($handler)->getParameters();
				$values = isset($params[1]) ? $this->getValues($params[1]->isArray()) : null;
				$handler($this, $values);
			}
		}

		if($this->isCli()){
			if(!in_array("cli",$list)){
				throw new AbortException("Cli Router not defined.");
			}
		}
		/** @var RouterFactory $routerFactory */
		$routerFactory = $this->container->getByType(RouterFactory::class);
		foreach($list as $key){
			if(!isset($this->routers[$key])){
				throw new AbortException("Router with '$key' not exists.",500);
			}
			if($this->routers[$key] !== null) {
				$selectedRouter = $routerFactory->getByType( $this->routers[ $key ] );
			} else {
				throw new AbortException("Router with key '$key' is not defined.");
			}
		}
	}

	/**
	 * @deprecated
	 * CLI router init
	 * @param $className string
	 */
	protected function routerCli($className){
		return;
	}

	/**
	 * API router init
	 * @param $classname string
	 *
	 * @throws Nette\Application\BadRequestException
	 */
	protected function routerApi($classname){
		/*if(!$this->isCli()) {
			if ( $this->getService( "request" )->getQuery( 'api' ) ) {
				$api = new $classname( $this->getService( "request" )->getQuery( "query" ), $this );
				exit;
			}
		}*/
		return;
	}

	/**
	 * @deprecated
	 * @param $classname
	 */
	protected function routerAjax($classname){
		return;
	}

	/**
	 * @deprecated
	 * @param $classname
	 */
	protected function routerRedirects($classname){
		return;
	}
	/**
	 * @deprecated
	 * Application router init
	 * @param $classname
	 */
	protected function routerApplication($classname){
		return;
	}
	public function __construct()
	{
		$this->parameters = $this->getDefaultParameters();
		if(!defined("APP_DIR") || !defined("WWW_DIR") || !defined("ASSETS_DIR") || !defined("TEMP_DIR")){
			throw new AbortException("Main dir paths is not defined!");
		}
	}

	public function createFunctionsLoader(array $list){
		foreach ( $list as $file )
		{
			if(!file_exists($file)){
				throw new \Exception("File '$file' not exists.");
			}
			include_once $file;
		}
	}

	/**
	 * checks if shop is protein
	 * @return bool
	 */
	public function isProtein(){
		$url = $this->parameters["shop"]["url"];
		return Strings::contains($url,"protein");
	}

	/**
	 * Is host
	 * @param $host
	 *
	 * @return bool
	 */
	public function isHost($host){
		/**
		 * only for debug
		 */
		if(!$this->isProduction()) {
			if ( $host == "protein.cz" ) {
				//return true;
			}
			if ( $host == "protein.sk" ) {
				return true;
			}
		} else {
			if(strpos($this->parameters["host"],$host) !== false){
				return true;
			}
		}
		return false;
	}

	/**
	 * Is console mode
	 * @return bool
	 */
	public function isCli(){
		return $this->parameters["consoleMode"];
	}

	/**
	 * Set parameter %debugMode%.
	 * @param  bool|string|array
	 * @return self
	 */
	public function setDebugMode($value)
	{
		if (is_string($value) || is_array($value)) {
			$value = static::detectDebugMode($value);
		} elseif (!is_bool($value)) {
			throw new Nette\InvalidArgumentException(sprintf('Value must be either a string, array, or boolean, %s given.', gettype($value)));
		}
		$this->parameters['debugMode'] = $value;
		$this->parameters['productionMode'] = !$this->parameters['debugMode']; // compatibility
		$this->parameters['environment'] = $this->parameters['debugMode'] ? 'development' : 'production';
		return $this;
	}


	/**
	 * @return bool
	 */
	public function isDebugMode()
	{
		return $this->parameters['debugMode'];
	}
	//public function is


	/**
	 * Sets path to temporary directory.
	 * @return self
	 */
	public function setTempDirectory($path)
	{
		$this->parameters['tempDir'] = $path;
		if(!file_exists($this->parameters["tempDir"])){
			FileSystem::createDir($this->parameters["tempDir"]);
		}
		$this->parameters["logDir"] = $path."/log";
		if(!file_exists($this->parameters["logDir"])){
			FileSystem::createDir($this->parameters["logDir"]);
		}
		return $this;
	}


	/**
	 * Adds new parameters. The %params% will be expanded.
	 * @return self
	 */
	public function addParameters(array $params)
	{
		$this->parameters =  DI\Config\Helpers::merge( $params, $this->parameters );
		return $this;
	}
	private function expandParams(array $params){

		$self = $this;
		array_walk_recursive($params, function (&$value){
			$re = '/\%([a-z][a-zA-Z-0-9_\.]+)\%/m';
			//$str = '%wwwDir%cert/comfortpay_novy.pem';
			
			if(is_string($value)) {
				preg_match_all($re, $value, $matches, PREG_SET_ORDER, 0);
				if ( ! empty($matches)) {
					//dump($matches);
					foreach ($matches as $param_array) {
						$param_name = $param_array[1];
						$res[]      = '/\%' . $param_name . '\%/';
						$explode    = explode(".", $param_name);
						if (count($explode) > 1) {
							$param[] = Arrays::getNestedValue($this->parameters_raw, $explode);
						} else {
							if (isset($this->parameters_raw[$param_name])) {
								$param[] = $this->parameters_raw[$param_name];
							}
						}


					}
					//bdump($param);
					//bdump($res);
					if ($param === null) {
						trigger_error("Bad parameter '$param_name' to replace.");
					} else {
						$value = preg_replace($res, $param, $value);
					}
					//
					//dump( preg_replace( $re, $this->parameters[$param_name], $value ), $this->parameters );
				}
			}
		});
		return $params;

	}
	protected function getDefaultParameters()
	{
		$trace = debug_backtrace(PHP_VERSION_ID >= 50306 ? DEBUG_BACKTRACE_IGNORE_ARGS : FALSE);
		$last = end($trace);
		$debugMode = static::detectDebugMode();
		return array(
			'appDir' => isset($trace[1]['file']) ? dirname($trace[1]['file']) : NULL,
			'wwwDir' => isset($last['file']) ? dirname($last['file']) : NULL,
			'debugMode' => $debugMode,
			'productionMode' => !$debugMode,
			'environment' => $debugMode ? 'development' : 'production',
			'consoleMode' => PHP_SAPI === 'cli',
			"host" => (PHP_SAPI === "cli")?"cli":$_SERVER["SERVER_NAME"],
			'container' => array(
				'class' => NULL,
				'parent' => NULL,
			),
		);
	}

	/**
	 * @param $key [database,latte]
	 *
	 * @return mixed
	 */
	public function getService($key){
		return $this->container->getService($key);
	}
	public function getByType(string $type){
		return $this->container->getByType($type);
	}

	/**
	 * @param  string        error log directory
	 * @param  string        administrator email
	 * @return void
	 */
	public function enableDebugger($logDirectory = NULL, $email = NULL)
	{
		if($email !== null){
			$this->setParameter(["adminEmail"],$email);
		}
		//dumpe($this->parameters["debugMode"]);
		//Tracy\Debugger::$strictMode = TRUE;
		Tracy\Debugger::enable(!$this->parameters['debugMode'], $logDirectory, $email);
		Tracy\Debugger::$errorTemplate = APP_DIR."view/error500.phtml";
		if(!empty(($logger = $this->container->findByType(Tracy\ILogger::class)))){
			Tracy\Debugger::setLogger($this->container->getService($logger[0]));
		}

		$this->debuggerEnabled = true;
		error_reporting(E_ALL & ~E_NOTICE);
		if(class_exists($this->debuggerPanelService)) {
			$tracyPanel = new $this->debuggerPanelService();
			$tracyPanel->diStartup( $this );
		}

	}

	public function afterAppLoad(){
		if($this->debuggerEnabled && class_exists($this->debuggerPanelService)){
			$tracyPanel = new $this->debuggerPanelService();
			$tracyPanel->diAfterLoad($this);
		}
		/** @var Nette\Database\Connection $database */
		//$database = $this->getByType(\Nette\Database\Connection::class);
		//mysqli_query(\CommerceDB::$DB, "SHOW TABLES");
		//dump(\CommerceDB::$DB,$database);

		/*$c = mysqli_close(\CommerceDB::$DB);
		\CommerceDB::$DB = null;
		$database->disconnect();*/


		//dump(\CommerceDB::$DB,$database,$c);
	}


	/**
	 * @return RobotLoader
	 * @throws Nette\NotSupportedException if RobotLoader is not available
	 */
	public function createRobotLoader()
	{
		if (!class_exists('Nette\Loaders\RobotLoader')) {
			throw new Nette\NotSupportedException('RobotLoader not found, do you have `nette/robot-loader` package installed?');
		}

		$loader = new RobotLoader;
		$loader->setTempDirectory($this->getCacheDirectory());
		//$loader->setCacheStorage(new Nette\Caching\Storages\FileStorage($this->getCacheDirectory()));
		//$loader->autoRebuild = $this->parameters['debugMode'];
		return $loader;
	}
	public function getCacheDirectory()
	{
		if (empty($this->parameters['tempDir'])) {
			throw new InvalidStateException('Set path to temporary directory using setTempDirectory().');
		}
		$dir = $this->parameters['tempDir'] . '/cache';
		if (!is_dir($dir)) {
			@FileSystem::createDir($dir); // @ - directory may already exist
		}
		return $dir;
	}

	public static function detectDebugMode($list = NULL)
	{
		//if(isset($_SERVER["HTTP_CDN_LOOP"]) && $_SERVER["HTTP_CDN_LOOP"] == 'cloudflare') {
		if(isset($_SERVER["HTTP_CDN_LOOP"]) && str_contains($_SERVER["HTTP_CDN_LOOP"], 'cloudflare')) {
			$addr = $_SERVER['HTTP_CF_CONNECTING_IP'];
			$_SERVER["REMOTE_ADDR"] = $addr;
		} elseif(isset($_SERVER["HTTP_X_REAL_IP"])) {
			$addr = $_SERVER["HTTP_X_REAL_IP"];
		} else {
			$addr = isset( $_SERVER['REMOTE_ADDR'] )
				? $_SERVER['REMOTE_ADDR']
				: php_uname( 'n' );
		}

		$secret = isset($_COOKIE[self::COOKIE_SECRET]) && is_string($_COOKIE[self::COOKIE_SECRET])
			? $_COOKIE[self::COOKIE_SECRET]
			: NULL;
		$list = is_string($list)
			? preg_split('#[,\s]+#', $list)
			: (array) $list;
		if (!isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$list[] = '127.0.0.1';
			$list[] = '::1';
		}
		return in_array($addr, $list, TRUE) || in_array("$secret@$addr", $list, TRUE);
	}

	/**
	 * @return array
	 * @throws Nette\Application\BadRequestException
	 */
	protected function cookiesBar(){
		$REQUEST = $this->getService("request");
		$cookie = [];
		$cookie["setted"] = (bool) $REQUEST->getCookie("cookies_setted");
		$cookie["technical"] = (bool)$REQUEST->getCookie("allow_technical_cookies");
		$cookie["analytics"] = (bool)$REQUEST->getCookie("allow_analytics_cookies");
		$cookie["marketing"] = (bool)$REQUEST->getCookie("allow_marketing_cookies");
		$cookie["scripts"]["css"] = file_get_contents(__DIR__."/assets/kubomikita.cookiebar.css");
		$cookie["scripts"]["js"] = file_get_contents(__DIR__."/assets/kubomikita.cookiebar.js");

		return $cookie;
	}

	public function addConfig($file){
		$this->files[] = [$file,(strpos($file,"local") ? 'development' : 'production')];
		foreach($this->files as $f){
			$content = FileSystem::read($f[0]);
			if(strlen(trim($content))>0) {
				$neon = Neon::decode( $content );
				if(!$this->isDebugMode() and ($f[1] == "development") and !$this->isCli() ){
					continue;
				}
				$this->addParameters($neon);
			}
		}
	}

	/**
	 * @param $section
	 * @param null $key
	 *
	 * @return mixed
	 * @throws AbortException
	 */
	public function getParameter($section,$key = null){
		if(!isset($this->parameters[$section])){
			throw new AbortException("Section '$section' in parameters not exists");
		}
		if(!isset($this->parameters[$section][$key]) and $key !== null){
			throw new AbortException("Key '$key' in section '$section' not exists");
		}
		if($key !== null){
			return $this->parameters[$section][$key];
		}
		return $this->parameters[$section];
	}

	/**
	 * @param array $section
	 * @param $value
	 *
	 * @return $this|IDiConfigurator
	 */
	public function setParameter(array $section, $value) {
		Arrays::setNestedValue($this->parameters_raw,$section,$value);
		$this->parameters = $this->expandParams($this->parameters_raw);
		$this->container->parameters = $this->parameters;
		return $this;
	}

	/**
	 * Check if app runing on localhost or production server
	 * @return bool
	 */
	public function isProduction(){
		if(!$this->isCli()) {
			if(Strings::endsWith($_SERVER["HTTP_HOST"],".local")){
				return false;
			}
			return ( $_SERVER["HTTP_HOST"] != "localhost" );

		} else {
			if(strpos(__DIR__,"E:\\www") !== false){
				return false;
			}
		}
		return true;
	}

	public static function internationalPhoneNumber($number){
		$val = str_replace(" ","",$number);
		if(!Strings::contains($val,"+421")){
			$val = "+421".Strings::substring($val,1);
		}
		return $val;
	}
	public static function getRelativePath( $from, $to ) {
		// some compatibility fixes for Windows paths
		$from = is_dir( $from ) ? rtrim( $from, '\/' ) . '/' : $from;
		$to   = is_dir( $to ) ? rtrim( $to, '\/' ) . '/' : $to;
		$from = str_replace( '\\', '/', $from );
		$to   = str_replace( '\\', '/', $to );

		$from    = explode( '/', $from );
		$to      = explode( '/', $to );
		$relPath = $to;

		foreach ( $from as $depth => $dir ) {
			// find first non-matching dir
			if ( $dir === $to[ $depth ] ) {
				// ignore this directory
				array_shift( $relPath );
			} else {
				// get number of remaining dirs to $from
				$remaining = count( $from ) - $depth;
				if ( $remaining > 1 ) {
					// add traversals up to first matching dir
					$padLength = ( count( $relPath ) + $remaining - 1 ) * - 1;
					$relPath   = array_pad( $relPath, $padLength, '..' );
					break;
				} else {
					$relPath[0] = './' . $relPath[0];
				}
			}
		}

		return implode( '/', $relPath );
	}

	protected function beforeAppLoad(){
		//header('Set-Cookie: cross-site-cookie=name; SameSite=None; Secure');
		bdump($this->container->hasService("controller"),'beforeAppLoad');
		if($this->container->hasService("controller")) {
			$this->controllers = $this->getService( "controller" )->create();
			//$this->getService("form.factory")->register();
		}
	}

	abstract protected function setMicrodata();

	/**
	 * @param \Page $Page
	 * @param AppRouter $Router
	 *
	 * @return bool|string
	 * @throws AbortException
	 * @throws \Nette\Application\BadRequestException
	 */
	abstract public function run( $Page, $Router );
}