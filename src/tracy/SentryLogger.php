<?php

namespace Commerce;



use Monolog\Utils;
use Nette\DI\Container;
use Nette\Security\User;
use Nette\Utils\ArrayHash;
use Sentry\EventHint;
use Sentry\Frame;
use Sentry\Integration\EnvironmentIntegration;
use Sentry\Integration\FrameContextifierIntegration;
use Sentry\Integration\ModulesIntegration;
use Sentry\SentrySdk;
use Sentry\Stacktrace;
use Tracy\ILogger;
use Tracy\Logger;
use Nette\Utils\Strings;
use Sentry\Integration\RequestIntegration;
use Sentry\Severity;
use Sentry\State\Scope;
use Throwable;
use Tracy\Debugger;
use Tracy\Dumper;

use function Sentry\captureException;
use function Sentry\captureMessage;
use function Sentry\configureScope;
use function Sentry\init;

class SentryLogger extends Logger {
	//private $user;
	//private $config = [];

	public function __construct(private User $user, private Container $container, private array $config =[]) {
		if(empty($this->config)){
			$this->config = $this->container->parameters["sentry"];
			$this->config["defaultLogger"] = $this->config["defaultLogger"] ?? false;
		}

		$init = [
			'dsn'                  => $this->config["dsn"],
			'environment'          => $this->config["environment"],
			'attach_stacktrace'    => $this->config["attach_stacktrace"] ?? true,
			'default_integrations' => false,
			'integrations'         => [
				new RequestIntegration(),
				new FrameContextifierIntegration(),
				new EnvironmentIntegration(),
				new ModulesIntegration(),
			],
		];
		if($release = $this->config["release"] ?? false){
			$init["release"] = $release;
		}
		init( $init );

		if($this->config["defaultLogger"]) {
			$this->email = &Debugger::$email;

		}
		//parent::sendEmail('test');

		$this->directory = Debugger::$logDirectory;
		//$this->user = $userService;
		//$this->config = $container->parameters["sentry"];
	}

	private function exclude( string $message) : bool {
		$exclude = $this->config["exclude"] ?? ["Notice", 'Warning: fopen'];
		foreach ($exclude as $item){
			if(Strings::contains($message, $item)){
				return true;
			}
		}

		return false;
	}


	public function log( $value, $level = self::INFO ) {
		$response = parent::log( $value, $level );
		$severity = $this->tracyPriorityToSentrySeverity( $level );
		//bdump($value, $level);
		//bdump($this);
		// if we still don't have severity, don't log anything
		if ( ! $severity ) {
			return $response;
		}



		$message = ( $value instanceof \Throwable ) ? $value->getMessage() : $value;

		if($this->config["syslog"] ?? true) {
			$this->logSyslog($value, $level);
		}
		bdump($message);

		if(Strings::contains($message, "PHP Warning:") || Strings::contains($message, "PHP Deprecated:")){
			$severity = Severity::warning();
		}


		bdump( $response, "Response" );
		bdump( $severity, "severity" );


		//bdump($value);

		if($this->exclude($message)){
			//bdump($this->config["exclude"], "EXCLUDED");
			return $response;
		}

		if ( Strings::contains( $message, "Notice" ) || Strings::contains($message, 'Warning: fopen') ) {
			return $response;
		}


		configureScope( function ( Scope $scope ) use ( $severity, $response ) {
			if ( ! $severity ) {
				return;
			}
			$scope->setLevel( $severity );
			$scope->setExtra( "blueScreen", $response );
			if($this->user->isLoggedIn()) {
				$scope->setUser([
					"id" => $this->user->getId(),
					"username" => $this->user->getIdentity()->username,
					"ip_address" => $_SERVER["REMOTE_ADDR"],
				]);
			} else {
				$scope->setUser([
					'ip_address' => $_SERVER["REMOTE_ADDR"] ?? '127.0.0.1',
				]);
			}
			$scope->setExtra("ip", $_SERVER["REMOTE_ADDR"] ?? '127.0.0.1');
			$scope->setExtra('session', $_SESSION);
			$scope->setExtra('post', $_POST);
			$scope->setExtra('files', $_FILES);
		} );


		if ( $value instanceof Throwable ) {
			captureException( $value );
		} else {
			$frames = [];
			$i = 0;

			$file = __FILE__;
			$line = __LINE__;
			foreach (debug_backtrace(0) as $row){
				$row = ArrayHash::from($row, false);
				if($i < 3){
					$i++;
					continue;
				}
				if($i === 3){
					$file = $row->file;
					$line = $row->line;
					unset($row->class);
				}
				$frames[] = (array) $row;
				$i++;
			}
			$hint = new EventHint();
			$hint->stacktrace = SentrySdk::getCurrentHub()->getClient()->getStacktraceBuilder()->buildFromBacktrace($frames, $file, $line);
			captureMessage( is_string( $value ) ? $value : Dumper::toText( $value ) ,$severity,  $hint);
		}
		return $response;
	}

	private function tracyPriorityToSentrySeverity( string $priority ): ?Severity {
		switch ( $priority ) {
			case ILogger::DEBUG:
				return Severity::debug();
			case ILogger::INFO:
				return Severity::info();
			case ILogger::WARNING:
				return Severity::warning();
			case ILogger::ERROR:
			case ILogger::EXCEPTION:
				return Severity::error();
			case ILogger::CRITICAL:
				return Severity::fatal();
			default:
				return null;
		}
	}

	private function logSyslog($error, $severity = Logger::WARNING): void
	{
		$logLevels = array(
			Logger::DEBUG => LOG_DEBUG,
			Logger::INFO           => LOG_INFO,
			Logger::WARNING        => LOG_WARNING,
			Logger::ERROR          => LOG_ERR,
			Logger::CRITICAL       => LOG_CRIT,
			Logger::EXCEPTION          => LOG_ERR,
		);
		if (!openlog($this->container->parameters["shop"]["name"], LOG_PID, LOG_USER)) {
			//throw new \LogicException('Can\'t open syslog for ident "'.$this->ident.'" and facility "'.$this->facility.'"');
		}

		if($error instanceof Throwable){
			$error = 'Exception: '.get_class($error).' (code: '.$error->getCode().'): '.$error->getMessage().' at '.$error->getFile().':'.$error->getLine().')';
		}
		syslog($logLevels[$severity], (string) $error);
		closelog();
	}




}
