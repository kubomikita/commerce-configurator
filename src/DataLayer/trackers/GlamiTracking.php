<?php

namespace Kubomikita\Commerce\DataLayer;

use Nette\Application\AbortException;
use Nette\Utils\Html;

class GlamiTracking  extends DataLayer {
	protected $products = [];
	protected $pageType;
	protected $price = 0;
	protected $action = 'glami("track", \'%s\', %s)';

	protected $transactionId;

	/**
	 * @param mixed $transactionId
	 */
	public function setTransactionId( $transactionId ): void {
		$this->transactionId = $transactionId;
	}
	/**
	 * @param string $pageType
	 */
	public function setPageType(string $pageType ) {
		$this->pageType = $pageType;
	}
	/**
	 * @param int $price
	 */
	public function setPrice( int $price ) {
		$this->price = $price;
	}
	/**
	 * @param array $products
	 */
	public function setProducts( array $products ) {
		$this->products = $products;
	}

	private function getProductIds(){

			$ids = [];
			foreach ( $this->products as $p ) {

				if($p instanceof \KosikItem || $p instanceof \ObjednavkaItem){
					//dump();
					$ids[] = $p->tovar->variant_id();//$p->tovar->id;
				} else {
					$ids[] = $p->id;
				}
			}

			return $ids;
		 /*
		if($this->products[0] instanceof \KosikItem || $this->products[0] instanceof \ObjednavkaItem){
			return $this->products[0]->tovar->id;
		} else {
			return $this->products[0]->id;
		}*/

	}
	private function getProductNames(){
		$names = [];
		foreach ($this->products as $p){
			if($p instanceof \KosikItem || $p instanceof \ObjednavkaItem){
				$names[] = (String) $p->tovar->nazov;
			} else {
				$names[] = (String) $p->nazov;
			}

		}
		return $names;
	}
	private function getProductPrice(){
		if(count($this->products) > 1){
			$prices = [];
			foreach($this->products as $p){
				if($p instanceof \KosikItem || $p instanceof \ObjednavkaItem){
					$prices[] = /*\Format::money_user_plain(*/(float)$p->tovar->predajna_cena()->suma() * $p->mnozstvo()/*)*/;
				} else {
					$prices[] = /*\Format::money_user_plain(*/(float)$p->predajna_cena()->suma()/*)*/;
				}

			}
			return \Format::money_user_plain(array_sum($prices));
		}
		if($this->products[0] instanceof \KosikItem || $this->products[0] instanceof \ObjednavkaItem){
			return \Format::money_user_plain( (float) $this->products[0]->tovar->predajna_cena()->suma() );
		} else {
			return \Format::money_user_plain( (float) $this->products[0]->predajna_cena()->suma() );
		}
	}

	public function track() : array {
		$this->setOutputType(Container::OUTPUT_GLAMI);

		if(!empty($this->products)) {

			$output["item_ids"]       = Container::arrayToJsArray($this->getProductIds());
			$output["product_names"]    = Container::arrayToJsArray($this->getProductNames());
			$output["value"] = $this->getProductPrice();
			$output["currency"] = "EUR";
			if($this->transactionId !== null){
				$output["transaction_id"] = $this->transactionId;
			}
			$this->output["row"] = sprintf($this->action,$this->pageType, str_replace(["\"['","']\""],["['","']"], Container::arrayToJsObject($output)));

		}
		return $this->output;
	}



}