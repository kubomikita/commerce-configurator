<?php

namespace Kubomikita\Commerce\DataLayer;

use Nette\Application\AbortException;
use Nette\Utils\Html;

class CriteoTracking  extends DataLayer {
	protected $items = [];
	protected $item;
	protected $event;
	protected $siteType;
	protected $account;
	protected $email;
	protected $transactionId;

	public function __construct(int $accountId = null) {
		$this->setAction('window.criteo_q.push(%s)');
		$this->setOutputType(\Kubomikita\Commerce\DataLayer\Container::OUTPUT_JSON_OBJECT);
		if($accountId !== null){
			$this->setAccount($accountId);
		}
		$this->setInsertBeforeScript('<script type="text/javascript" src="//static.criteo.net/js/ld/ld.js" async="true"></script>');
		$mobiledetect = (new \Detection\MobileDetect());
		$siteType = function () use($mobiledetect){
			if($mobiledetect->isTablet()){
				return "t";
			} elseif ($mobiledetect->isMobile()){
				return "m";
			}
			return "d";
		};
		$this->setSiteType($siteType());
		$this->setInsertBefore('window.criteo_q = window.criteo_q || [];'."\n");
	}

	public function track(): array {
		$this->output[] = ["event" => "setAccount", "account" => $this->account];
		$this->output[] = ["event" => "setSiteType", "type" => $this->siteType];
		if($this->email !== null){
			$this->output[] = ["event" => "setEmail", "email" => md5($this->email)];
		} else {
			$this->output[] = ["event" => "setEmail", "email" => ""];
		}
		if(!empty($this->items)) {
			$items = [ "event" => $this->event, "item" => $this->items ];
			if($this->transactionId !== null){
				$items["id"] = $this->transactionId;
			}
			$this->output[] = $items;
		} elseif($this->item !== null) {
			$this->output[] = [ "event" => $this->event, "item" => $this->item ];
		} else {
			$this->output[] = [ "event" => $this->event ];
		}
		return $this->output;
	}

	/**
	 * @param mixed $event
	 * @param array $items
	 */
	public function setEvent( $event , $items = [] ) {
		$this->event = $event;
		$this->items = $items;
	}

	/**
	 * @param mixed $siteType
	 */
	public function setSiteType( $siteType ) {
		$this->siteType = $siteType;
	}

	/**
	 * @param mixed $account
	 */
	public function setAccount( $account ) {
		$this->account = $account;
	}

	/**
	 * @param mixed $email
	 */
	public function setEmail( $email ) {
		$this->email = $email;
	}

	/**
	 * @param mixed $item
	 *
	 * @return CriteoTracking
	 */
	public function setItem( $item ) {
		$this->item = $item;

		return $this;
	}

	/**
	 * @param mixed $transactionId
	 *
	 * @return CriteoTracking
	 */
	public function setTransactionId( $transactionId ) {
		$this->transactionId = $transactionId;

		return $this;
	}

}