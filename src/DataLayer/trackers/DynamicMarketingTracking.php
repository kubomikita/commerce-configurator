<?php

namespace Kubomikita\Commerce\DataLayer;

use Nette\Application\AbortException;
use Nette\Utils\Html;

class DynamicMarketingTracking  extends DataLayer {
	protected $products = [];
	protected $pageType;
	protected $price = 0;

	/**
	 * @param string $pageType
	 */
	public function setPageType(string $pageType ) {
		$this->pageType = $pageType;
	}
	/**
	 * @param int $price
	 */
	public function setPrice( int $price ) {
		$this->price = $price;
	}
	/**
	 * @param array $products
	 */
	public function setProducts( array $products ) {
		$this->products = $products;
	}

	private function getProductIds(){
		if(count($this->products) > 1) {
			$ids = [];
			foreach ( $this->products as $p ) {
				if($p instanceof \KosikItem || $p instanceof \ObjednavkaItem){
					$ids[] = $p->tovar->id;
				} else {
					$ids[] = $p->id;
				}
			}

			return $ids;
		}
		if($this->products[0] instanceof \KosikItem || $this->products[0] instanceof \ObjednavkaItem){
			return $this->products[0]->tovar->id;
		} else {
			return $this->products[0]->id;
		}

	}
	private function getProductPrice(){
		if(count($this->products) > 1){
			$prices = [];
			foreach($this->products as $p){
				if($p instanceof \KosikItem || $p instanceof \ObjednavkaItem){
					$prices[] = /*\Format::money_user_plain(*/(float)$p->tovar->predajna_cena()->zaklad() * $p->mnozstvo()/*)*/;
				} else {
					$prices[] = /*\Format::money_user_plain(*/(float)$p->predajna_cena()->zaklad()/*)*/;
				}

			}
			return \Format::money_user_plain(array_sum($prices));
		}
		if($this->products[0] instanceof \KosikItem || $this->products[0] instanceof \ObjednavkaItem){
			return \Format::money_user_plain( (float) $this->products[0]->tovar->predajna_cena()->zaklad() );
		} else {
			return \Format::money_user_plain( (float) $this->products[0]->predajna_cena()->zaklad() );
		}
	}

	public function track() : array {

		if(!empty($this->products)) {

			$this->output["ID"]       = $this->getProductIds();
			$this->output["Price"]    = $this->getProductPrice();
			$this->output["PageType"] = $this->pageType;


		}
		return $this->output;
	}



}