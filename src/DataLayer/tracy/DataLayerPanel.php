<?php

namespace Kubomikita\Commerce;

use Kubomikita\Commerce\DataLayer\Container;
use Kubomikita\Service;
use Tracy\IBarPanel;

class DataLayerPanel implements IBarPanel{
	const DATALAYER_ICON = '<svg enable-background="new 0 0 128 128" id="Layer_1" version="1.1" viewBox="0 0 128 128" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><circle cx="64" cy="64" fill="#4B5F83" id="circle" r="64"/><g id="icon"><g id="_x30_3"><polygon id="shadow_2_" opacity="0.2" points="31.5,77.3 64,57.8 96.5,77.3 64,96.8   "/><polygon fill="#E67E22" id="rhombus_2_" points="31.5,73 64,53.5 96.5,73 64,92.5   "/></g><g id="_x30_2"><polygon id="shadow_1_" opacity="0.2" points="31.5,66.1 64,46.7 96.5,66.1 64,85.6   "/><polygon fill="#CF000F" id="rhombus_1_" points="31.5,61.9 64,42.4 96.5,61.9 64,81.3   "/></g><g id="_x30_1"><polygon id="shadow" opacity="0.2" points="31.5,55 64,35.5 96.5,55 64,74.5   "/><polygon fill="#22A7F0" id="rhombus" points="31.5,50.7 64,31.2 96.5,50.7 64,70.2   "/></g></g></svg>';
	private $container = null;
	public function __construct(Container $container) {
		$this->container = $container;
	}

	public function getPanel() {
		/*$sections = $this->microdata->getSection();
		ob_start(function () {});
		require __DIR__ . '/templates/DataLayerPanel.panel.phtml';
		return ob_get_clean();*/
		return "";
	}
	public function getTab() {
		$count = $this->container->count();
		ob_start(function () {});
		echo '<span title="DataLayer">
			    '.self::DATALAYER_ICON.'
			    <span class="tracy-label">'.$count.'</span>
			</span>';
		return ob_get_clean();
	}
}