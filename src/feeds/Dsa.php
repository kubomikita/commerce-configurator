<?php

namespace Commerce\Csv;
use Nette\Database\Context;
use Nette\Utils\Strings;


class Dsa extends CsvFeed {

	//public $debug = true;

	public function __construct() {
		parent::__construct();
		$this->addHeader('Content-Disposition: attachment; filename=dsa-'.date("Ymdhis").'.csv');
	}

	public function getCsv(array $query = []):string {
		$db = $this->container->getByType(Context::class);
		$shopUrl = $this->container->parameters["shop"]["url"];

		$kat[0] = array("Page URL","Custom label");
		$q = $db->table("ec_tovar_search_cache")->where(["aktivny" => 1, "indexable" => 1]);
		foreach($q as $r){
			$t =new \Tovar($r->tovar_id);
			if($t->vyrobca->id > 0) {
				$stock = \Config::kosikCheckSklad ? $t->sklad_available() : 1;

				$kategoria = array();
				$k = $t->primarna_kategoria->fetchPath();
				foreach($k as $ka){
					if($ka->level() > 0){
						$kategoria[] = (string) $ka->nazov;
					}
				}
				$kat["product_". $t->id ]["link"]   = $shopUrl.eshop_seo_link( $t );
				$kat["product_". $t->id ]["labels"] = 'Product; '.trim(implode(" > ",$kategoria)).'; '.$t->vyrobca->nazov."; ".($stock > 0 ? 'in stock' : 'out stock');
			}
		}
		/** @var \Kategoria $K */
		foreach (\Kategoria::fetchList() as $K){
			if($K->level() > 0 && $K->visible && $K->countActiveItems() > 0) {
				$kategoria = [];
				$k = $K->fetchPath();
				foreach($k as $ka){
					if($ka->level() > 0){
						$kategoria[] = (string) $ka->nazov;
					}
				}
				$kat["category_".$K->id]["link"] = $shopUrl.$K->link();
				$kat["category_".$K->id]["labels"] = 'Category; '.trim(implode(" > ",$kategoria)).'; all; in stock';
				//dump( $K );
			}
		}
		
		ob_start();
		$out = fopen('php://output', 'w');
		$a = [];
		foreach($kat as $cid => $v){
			//fputcsv($out,  array_values($v), ","/*, ''*/);
			$this->fputcsv($out, array_values($v), ",");
		}
		fclose($out);
		return ob_get_clean();
	}
}