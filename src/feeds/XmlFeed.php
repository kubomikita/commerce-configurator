<?php


namespace Commerce\Xml;

use Commerce\Feeds\Feed;
use Kubomikita\Service;
use Nette\Http\Request;
use Nette\InvalidStateException;
use Nette\Utils\Strings;

abstract class XmlFeed extends Feed {

	/** @var \DOMDocument */
	protected $xmlDocument;

	public function __construct()
	{
		parent::__construct();
		$this->xmlDocument = new \DOMDocument('1.0', 'utf-8');
		$this->xmlDocument->formatOutput = true;
		$this->setType("xml");
		$this->addHeader('Content-type: text/xml');
		
	}


	/**
	 * @return string
	 */
	abstract public function getXml(array $query = []);
	public function saveXml(string $file, array $query = []) {
		return $this->saveFeed($query);
	}
	public function getFeed( array $query = [] ): string {
		return $this->getXml($query);
	}

	/**
	 * @param string
	 * @param string
	 * @return \DOMElement
	 */
	protected function createElement($name, $cdata)
	{
		$el = $this->xmlDocument->createElement($name);
		if ($cdata !== NULL) {
			$el->appendChild($el->ownerDocument->createCDATASection($cdata));
		}
		return $el;
	}
}