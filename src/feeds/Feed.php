<?php

namespace Commerce\Feeds;


use Kubomikita\Service;
use Nette\DI\Container;
use Nette\Http\Request;
use Nette\InvalidStateException;
use Nette\Utils\FileSystem;
use Nette\Utils\Strings;

abstract class Feed {
	/** @var string  */
	protected $feedDir;
	/** @var string  */
	protected $feedName;
	/** @var string  */
	protected $feedFile;
	/** @var bool  */
	protected $debug = false;
	/** @var Container */
	protected $container;
	/** @var Request */
	protected $httpRequest;
	/** @var string[] */
	protected $headers = [];
	/** @var string[] */
	protected $allowedQuery = [];
	/** @var string */
	protected $type;

	public function __construct() {
		$this->container = Service::getByType(Container::class);
		$this->httpRequest = $this->container->getByType(Request::class);

		$reflection = new \ReflectionClass($this);

		$this->feedDir = TEMP_DIR."export/";
		$this->feedName = Strings::lower($reflection->getShortName());
		if($this->getQuery() !== []) {
			$this->feedName .= '-' . md5(json_encode($this->getQuery()));
		}
		$this->feedFile = $this->feedDir.$this->feedName; //.".csv";
	}

	abstract public function getFeed(array $query = []) : string ;

	public function saveFeed(array $query = []) : string {
		$p = pathinfo($this->feedFile);
		if(!file_exists($p["dirname"])){
			FileSystem::createDir($p["dirname"]);
		}
		$content = $this->getFeed($query);
		//echo $this->feedFile."-".strlen($content);
		FileSystem::write($this->feedFile , $content);
		return $content;
	}

	public function render() : ?string{
		if(file_exists($this->feedFile) && empty($this->getQuery()) && !$this->debug){
			return FileSystem::read($this->feedFile);
		} else {
			if(!$this->debug) {
				if ( empty( $this->getQuery() ) /*and empty( $this->isAllowedQuery() )*/ ) {
					throw new InvalidStateException( "Feed '{$this->feedName}' is not generated by Cron" );
				}
				$xml = $this->saveFeed( $this->getQuery() );

				return $xml;
			} else {
				$xml = $this->saveFeed( $this->getQuery() );

				return $xml;
			}
		}
	}

	protected function addHeader($header) : void {
		array_push($this->headers, $header);
	}

	protected function getQuery() : array{
		$query = [];
		if(empty($this->isAllowedQuery())){
			return [];
		}
		$default = ["api","query","name"];
		foreach($this->httpRequest->getQuery() as $k=>$v){
			if(!in_array($k,$default) && in_array($k, $this->isAllowedQuery())){
				$query[$k] = $v;
			}
		}
		return $query;
	}
	protected function isAllowedQuery() : array{
		return $this->allowedQuery;
	}

	/**
	 * @param string[] $allowedQuery
	 */
	public function setAllowedQuery( array $allowedQuery ): void {
		$this->allowedQuery = $allowedQuery;
	}
	/**
	 * @param string $type
	 */
	public function setType( string $type ): void {
		$this->feedFile .= ".".$type;
		$this->type = $type;
	}

	public function headers(){
		foreach($this->headers as $h){
			header($h);
		}
	}

}