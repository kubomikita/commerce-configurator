<?php


namespace Commerce\Xml;


class HeurekaStocks extends XmlFeed{

	public function getXml(array $query = []) {
		ob_start();
		echo('<'.'?xml version="1.0" encoding="utf-8"?'.'>'."\n".'<item_list>');

		$vyrobcovia = array();
		foreach(\TovarVyrobca::fetch() as $TV){
			if($TV->heureka_xml){
				$vyrobcovia[] = $TV->id;
			}
		}
		$Search = new \TovarSearchResult();$i=0;
		$Search->vyrobca = $vyrobcovia;

		$q = mysqli_query(\CommerceDB::$DB, $Search->query());
		$i = 0;
		while($r = mysqli_fetch_assoc($q)){

			$T = new \Tovar($r["tovar_id"]);
			if($T->visible_show()){
				$Var_list=$T->sablona->varianty_list();
				foreach($T->varianty_fetch_obj('all') as $V){
					$visible=$V->visible_show();
					if(\Config::variantHideInactivePublic){ $visible=$visible && $V->V_aktivny; };
					if($V->predajna_cena_user()==0){ $visible=0; };
					if($visible){
						$var_nazov = array(); $var_ids = array();$parm="";
						//var_dump($Var_list);
						foreach($Var_list as $TTV){ if($T->varianty_used($TTV->ident)){
							$var_nazov[]=$TTV->nazov.': '.$V->Variant($TTV->ident)->nazov;
							$var_ids[]=$V->Variant($TTV->ident)->id;
						}; };
					}

					$sklad = $V->sklad_available();
					//var_dump($sklad);
					if($sklad > 0){
						//echo '<ITEM_ID>'.$T->id.join("",$var_ids).'</ITEM_ID>';
						echo '<item id="'. htmlspecialchars( $T->id.(!empty($var_ids)?"-":"").join("-",$var_ids) ).'">'."\r\n";
						echo '<stock_quantity>'.$V->sklad_available().'</stock_quantity>'."\r\r\n";
						$delivery_dead =  date("Y-m-d")." 14:00";
						$delivery = date("Y-m-d",strtotime("+1 DAY"))." 17:00";
						if(strtotime($delivery_dead)<time()){
							$delivery_dead =  date("Y-m-d",strtotime("+1 DAY"))." 14:00";
							$delivery = date("Y-m-d",strtotime("+2 DAY"))." 17:00";
							//if($sklad > 0){


						}
						if(date("w",strtotime($delivery_dead)) == 5){
							$delivery = date("Y-m-d",strtotime("next monday"))." 17:00";
						}
						if(date("w",strtotime($delivery)) == 0 or date("w",strtotime($delivery))==6){
							$delivery = date("Y-m-d",strtotime("next tuesday"))." 17:00";
						}

						echo '<delivery_time orderDeadline="'.$delivery_dead.'">'.$delivery.'</delivery_time>'."\r\r\n";


						echo '</item>'."\r\n";
					}
				}
			}
		};

		echo('</item_list>');

		$xml=ob_get_contents();
		ob_end_clean();
		//Cache::put('tovar','xml_heureka_dostupnost',$xml);
		return $xml;
	}

}