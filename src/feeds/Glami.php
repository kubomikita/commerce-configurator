<?php


namespace Commerce\Xml;


use Kubomikita\Commerce\ProteinConfigurator;
use Kubomikita\Service;

class Glami extends XmlFeed {
	/** @var ProteinConfigurator */
	private $context;
	/** @var array */
	private $config;

	//protected $debug = true;

	public function getXml(array $query = []) {
		$this->context = Service::get("container");
		$this->config = $this->context->getParameter("glami");

		$domtree = new \DOMDocument("1.0","UTF-8");
		$domtree->preserveWhiteSpace = false;
		$domtree->formatOutput = true;

		$xmlRoot = $domtree->createElement("SHOP");
		$xmlRoot = $domtree->appendChild($xmlRoot);

		$Search = new \TovarSearchResult();$i=0;

		$Search->aktivny = 1;
		$Search->indexable = 1;
		$Search->visible = 1;
		$Search->hidden = 1;
		//$Search->id = [253,23410,1113];
		file_put_contents(TEMP_DIR."export/glami_xml_count",$Search->count());

		foreach($Search->netteQuery() as $row){

			$T = new \Tovar($row->tovar_id);
				if($this->context->isHost("papi.sk")) {
					$tovary = [ $T, $T->getChildren() ];
				} else {
					$tovary = [$T];
				}

				$Var_list=$T->sablona->varianty_list();
				foreach($T->varianty_fetch_obj('all') as $V){
					$visible=$V->visible_show();
					if(\Config::variantHideInactivePublic){ $visible=$visible && $V->V_aktivny; };
					if($V->predajna_cena_user()==0){ $visible=0; };
					if($visible){
						$shopitem = $domtree->createElement( "SHOPITEM" );
						$shopitem = $xmlRoot->appendChild( $shopitem );
						$var_nazov = $var_link = array(); $var_ids = array();
						$params = [];
						foreach($Var_list as $TTV){ if($T->varianty_used($TTV->ident)){
							$var_nazov[]=$V->Variant($TTV->ident)->nazov;
							$var_ids[]=$V->Variant($TTV->ident)->id;
							$var_link["variant"][$TTV->ident] = $V->Variant($TTV->ident)->id;

							$item = $domtree->createElement( "PARAM" );
							$item->appendChild( $domtree->createElement( "PARAM_NAME", /*htmlspecialchars($TTV->nazov)*/"veľkosť" ) );
							$item->appendChild( $domtree->createElement( "VAL",
								htmlspecialchars($V->Variant($TTV->ident)->nazov) ) );

							$params[] = $item;

						}; };
						if(isset($T->parametre["Farba"]) && strlen(trim($T->parametre["Farba"])) > 0 ){
							$item = $domtree->createElement("PARAM");
							$item->appendChild($domtree->createElement("PARAM_NAME","FARBA"));
							$item->appendChild($domtree->createElement("VAL",htmlspecialchars($T->parametre["Farba"])));
							$params[] = $item;
						}
						if(isset($T->parametre["Materiál"]) && strlen(trim($T->parametre["Materiál"])) > 0 ){
							$item = $domtree->createElement("PARAM");
							$item->appendChild($domtree->createElement("PARAM_NAME","MATERIAL"));
							$item->appendChild($domtree->createElement("VAL",htmlspecialchars($T->parametre["Materiál"])));
							$params[] = $item;
						} else {
							if(isset($T->parametre["Zvršok"]) && strlen(trim($T->parametre["Zvršok"])) > 0 ){
								$item = $domtree->createElement("PARAM");
								$item->appendChild($domtree->createElement("PARAM_NAME","MATERIAL"));
								$item->appendChild($domtree->createElement("VAL",htmlspecialchars($T->parametre["Zvršok"])));
								$params[] = $item;
							}
						}


						$shopitem->appendChild( $domtree->createElement('ITEM_ID', htmlspecialchars( $T->id.(!empty($var_ids)?"-":"").join("-",$var_ids) ) ) );
						$shopitem->appendChild( $domtree->createElement("ITEMGROUP_ID", $T->id));
						$shopitem->appendChild( $domtree->createElement('PRODUCT', htmlspecialchars($T->nazov) ) ); //.' '.join(' ',$var_nazov)
						//$shopitem->appendChild( $domtree->createElement( 'PRODUCTNAME', htmlspecialchars($T->nazov) ) ); // .' '.join(' ',$var_nazov)
						//if($this->context->isHost("kabelky-topanky.sk")){
							$popis = trim(strip_tags($T->popis));
							if(strlen($popis) > 0){
								$shopitem->appendChild( $domtree->createElement( 'DESCRIPTION',
									htmlspecialchars( trim(strip_tags( $popis )) ) ) );
							} else {
								$shopitem->appendChild( $domtree->createElement( 'DESCRIPTION',
									htmlspecialchars( strip_tags( $T->intro ) ) ) );
							}
						/*} else {
							$shopitem->appendChild( $domtree->createElement( 'DESCRIPTION',
								htmlspecialchars( strip_tags( $T->intro ) ) ) );
						}*/
						$shopitem->appendChild( $domtree->createElement( 'URL',
							htmlspecialchars($this->context->getParameter( "shop", "url" ) . $T->link().(!empty($var_link["variant"]) ? '?'.http_build_query($var_link) : "" ) )));
						$shopitem->appendChild( $domtree->createElement( 'IMGURL',
							$this->context->getParameter( "shop", "url" ) . $T->image()->cachedUrl() ) );
						//$shopitem->appendChild( $domtree->createElement( 'PRICE',
						//	strtr( sprintf( "%.2F", $T->predajna_cena_user()->zaklad() ), array( '.' => ',' ) ) ) );
						$shopitem->appendChild( $domtree->createElement( 'PRICE_VAT',
							strtr( sprintf( "%.2F", $T->predajna_cena_user()->suma() ), array( '.' => ',' ) ) ) );
						//$shopitem->appendChild( $domtree->createElement( 'VAT',
					//		strtr( sprintf( "%.2F", $T->predajna_cena_user()->dph() ), array( '.' => ',' ) ) ) );
						$shopitem->appendChild( $domtree->createElement( 'MANUFACTURER',
							htmlspecialchars( $T->vyrobca->nazov ) ) );
					//	$shopitem->appendChild( $domtree->createElement( 'ITEM_TYPE', 'new' ) );

						$kategorie = [];
						/** @var \Kategoria $Kp */
						foreach ( $T->primarna_kategoria->fetchPath() as $Kp ) {
							if ( $Kp->id != 1 ) {
								$kategorie[] = $Kp->alt_nazov( 'heureka_sk' );
							}
						}
						$category_text = htmlspecialchars( join( ' | ', $kategorie ) );
						$shopitem->appendChild( $domtree->createElement( 'CATEGORYTEXT', $category_text ) );
						//$shopitem->appendChild( $domtree->createElement( 'EAN', htmlspecialchars( strip_tags( $T->ean ) ) ) );
						$shopitem->appendChild( $domtree->createElement( 'DELIVERY_DATE', $this->getDeliveryDate($V) ) );
						if($V->variant_gift()->id>0){
							$shopitem->appendChild( $domtree->createElement( 'GIFT', $V->variant_gift()->nazov ) );
						}
						foreach($params as $p){
							$shopitem->appendChild($p);
						}
					}
				}


		}
		$xml = $domtree->saveXML();

		return $xml;
	}
	public function getDeliveryDate(\Tovar $T){
		/*$config = $this->config;
		if(isset($config["delivery_date"]) && $config["delivery_date"] !== null){
			return $config["delivery_date"];
		}*/
		if($T->sklad_stav() > 0){
			return 0;
		}
		return "";
	}

}