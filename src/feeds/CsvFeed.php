<?php

namespace Commerce\Csv;
use Commerce\Feeds\Feed;
use Kubomikita\Service;
use Nette\DI\Container;
use Nette\InvalidStateException;
use Nette\Utils\Strings;

abstract class CsvFeed extends Feed {


	public function __construct() {
		parent::__construct();
		$this->setType("csv");
		$this->addHeader('Content-type: text/csv; charset=utf-8');
	}

	/**
	 * @return string
	 */
	abstract public function getCsv(array $query = []) : string;

	public function getFeed( array $query = [] ): string {
		return $this->getCsv($query);
	}
	public function saveCsv(string $file, array $query = []) : string {
		return $this->saveFeed($query);
	}
	protected function fputcsv(&$handle, $fields = array(), $delimiter = ';', $enclosure = '') {
		$str = '';
		$escape_char = '\\';
		foreach ($fields as $value) {
			if (strpos($value, $delimiter) !== false ||
			    /*strpos($value, $enclosure) !== false ||*/
			    strpos($value, "\n") !== false ||
			    strpos($value, "\r") !== false ||
			    strpos($value, "\t") !== false ||
			    strpos($value, ' ') !== false) {
				$str2 = $enclosure;
				$escaped = 0;
				$len = strlen($value);
				for ($i=0;$i<$len;$i++) {
					if ($value[$i] == $escape_char) {
						$escaped = 1;
					} else if (!$escaped && $value[$i] == $enclosure) {
						$str2 .= $enclosure;
					} else {
						$escaped = 0;
					}
					$str2 .= $value[$i];
				}
				$str2 .= $enclosure;
				$str .= $str2.$delimiter;
			} else {
				$str .= $value.$delimiter;
			}
		}
		$str = substr($str,0,-1);
		$str .= "\n";
		return fwrite($handle, $str);
	}
}