<?php

namespace Kubomikita\Commerce;

interface IConfigurator {
	/**
	 * @param Page Page
	 * @param AppRouter Router
	 * @return string - renders to output
	 */
	public function run($Page, $Router);
	public function setTempDirectory($path);
	public function setDebugMode($value);
	public function isDebugMode();
	public function isCli();
	public function isHost($host);
	public function createRegister(array $list =[]);
	public function enableDebugger($logDirectory = null, $email = null);
	public function loadService($c);
	public function getService($key);

}