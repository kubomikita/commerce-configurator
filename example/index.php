<?php

include __DIR__."/../vendor/autoload.php";

try {
	$configurator = new Kubomikita\Commerce\Configurator();
	$configurator->setTempDirectory(__DIR__ . '/temp');
	$configurator->setDebugMode("217.31.42.54");
	$configurator->enableDebugger(__DIR__ . '/temp/log');
	$configurator->createRobotLoader()->addDirectory(__DIR__."/Services")->register();
	$configurator->createRegister(["latte","url","request"]); // ,"url","request","response","session","user","gettext","database","form-factory"
	$configurator->createRouter(["cli","api"]);
	dump($configurator);
	$configurator->setAppRouter("\\Kubomikita\\MyRouter");
	dump($configurator);

} catch(\Nette\Application\BadRequestException $e){
	dump($e);
} catch(Exception $e){
	echo $e->getMessage();
}