<?php


namespace Kubomikita\Commerce;


class LatteService implements IService{

	public function startup($configurator){
		$cacheDir = $configurator->getCacheDirectory();

		$latte = new \Latte\Engine;
		$latte->setTempDirectory($cacheDir);
		$latte->addFilter("translate",function($string){
			return __($string);
		});
		$set = new \Latte\Macros\MacroSet($latte->getCompiler());
		$set->addMacro("cache",function($node,$writer){
			$ret = $writer->write("if (Cache::check(%node.args)): ");
			$ret .= $writer->write("echo Cache::get(%node.args);");
			$ret .= $writer->write("else:");
			$ret .= $writer->write("ob_start();");
			return $ret;
		},function($node,$writter){
			$ret = $writter->write('$output = ob_get_clean();');
			$ret .= $writter->write('echo $output;');
			$ret .= $writter->write('Cache::put(%node.args,$output);');
			$ret .= $writter->write("endif");
			return $ret;
		});
		return $latte;
	}
}