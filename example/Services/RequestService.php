<?php

namespace Kubomikita\Commerce;

use Nette;
class RequestService implements IService{

	public function startup($configurator){

		$ip = (isset($_SERVER["HTTP_X_REAL_IP"]))?$_SERVER["HTTP_X_REAL_IP"]:$_SERVER["REMOTE_ADDR"];

		return new Nette\Http\Request(\Registry::get("url"),null,$_POST,$_FILES,$_COOKIE,apache_request_headers(),null,$ip);
	}
}