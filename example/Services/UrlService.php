<?php


namespace Kubomikita\Commerce;

use Nette;

class UrlService implements IService{

	public function startup($configurator){
		$url = new Nette\Http\UrlScript($_SERVER["REQUEST_SCHEME"]."://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"]);
		$url->setQuery($_GET);
		return $url;
	}
}